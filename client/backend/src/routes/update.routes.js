import { Router } from 'express'
import fetch from 'node-fetch'
import Createdata from '../models/create_event_data'
import {URLBS} from '../globalV'
import moment from 'moment'
const router = Router()

router.post('/', async (req, res) =>{

  var structureRes = {

    "id": "PT_CC8Project212",
    "url": URLBS,
    "date": moment().utc(true).toISOString(),
    "status": "OK"
  }

  const {id, url, date, update} = req.body
  console.log("---UPDATE---")
  console.log("id: " + id)
  console.log("url: " + url)
  console.log("date: " + date)
  console.log("update.id: " + update.id)

  //Enviando actualizacion del evento a base station
  await fetch( 'http://' + URLBS + '/update', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    },
    body: JSON.stringify(req.body)
  })
  .then( resFetch => resFetch.json())
  .then( datos => {
    //Verificando que base station haya hecho la actualizacion correctamente
    if(datos.status === 'ERROR') {
      structureRes.status = datos.status
    }
  })
  .catch( err => console.log("Error fetch to Base Station - Create Event: " + err))

  if(structureRes.status === 'OK'){

    const docFound = await Createdata.findOne({idEvent: update.id}, (err, resFind) => {
      if(err) {
        console.log(err)
        structureRes.status = "ERROR"
      }

      if(!resFind) {
        console.log("ERROR doc not found for update")
        structureRes.status = "ERROR"
      }
    })

    var createToUpdate = {
      if: {},
      then: {},
      else: {}
    }

    if(update.if) { createToUpdate.if = update.if }
    else { createToUpdate.if = docFound.create.if }

    if(update.then) {createToUpdate.then =  update.then }
    else { createToUpdate.then = docFound.create.then }

    if(update.else) {createToUpdate.else = update.else}
    else { createToUpdate.else = docFound.create.else }

    //Actualizando evento backup
    await docFound.updateOne({create: createToUpdate})

  }

  res.status(200).json(structureRes)

})

export default router
