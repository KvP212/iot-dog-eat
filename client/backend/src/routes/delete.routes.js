import { Router } from 'express'
import fetch from 'node-fetch'
import Createdata from '../models/create_event_data'
import {URLBS} from '../globalV'
import moment from 'moment'
const router = Router()

router.post('/', async (req, res) =>{

  var structureRes = {

    "id": "PT_CC8Project212",
    "url": URLBS,
    "date": moment().utc(true).toISOString(),
    "status": "OK"
  }

  const {id, url, date} = req.body
  const del = req.body.delete.id

  console.log("---DELETE---")
  console.log("id: " + id)
  console.log("url: " + url)
  console.log("date: " + date)
  console.log("delete.id: " + del)

  //Eliminando evento de base station
  await fetch( 'http://' + URLBS + '/delete', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    },
    body: JSON.stringify(req.body)
  })
  .then( resFetch => resFetch.json())
  .then( datos => {
    //Verificando que base station haya hecho la actualizacion correctamente
    if(datos.status === 'ERROR') {
      structureRes.status = datos.status
    }
  })
  .catch( err => console.log("Error fetch to Base Station - Create Event: " + err))

  if( structureRes.status === 'OK'){
    //Eliminando evento
    await Createdata.findOneAndRemove({idEvent: del}, (err, resFind) => {
      if(err) {
        console.log(err)
        structureRes.status = "ERROR"
      }

      if(!resFind) {
        console.log("ERROR doc not found for update")
        structureRes.status = "ERROR"
      }
    })
  }


  res.status(200).json(structureRes)

})

export default router
