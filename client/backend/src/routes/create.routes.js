import { Router } from 'express'
import fetch from 'node-fetch';
import Createdata from '../models/create_event_data'
import {URLBS} from '../globalV'
import moment from 'moment'
const router = Router()

//GET todos los Eventos
router.get('/', async(req, res) => {
  const docsFounds = await Createdata.find()
  res.json(docsFounds)
})

router.post('/', async (req, res) =>{

  var structureRes = {

    "id": "PT_CC8Project212",
    "url": URLBS,
    "date": moment().utc(true).toISOString(),
    "status": "OK",
    "idEvent": ""
  }

  const {id, url, date, create} = req.body
  console.log("---CREATE---")
  console.log("id: " + id)
  console.log("url: " + url)
  console.log("date: " + date)
  console.log("create: " + create)

  //Enviando evento a base station
  await fetch( 'http://' + URLBS + '/create', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    },
    body: JSON.stringify(req.body)
  })
  .then( resFetch => resFetch.json())
  .then( async datos => {

    if(datos.status === 'ERROR'){
      structureRes.status = 'ERROR'
    }

    //Creando evento backup con el id del evento creado en base station
    if(datos.idEvent) {

      structureRes.idEvent = datos.idEvent

      const createdatas = new Createdata({
        idFE: id,
        url,
        date,
        create,
        idEvent: datos.idEvent
      })

      //Guardando copia del evento creado
      await createdatas.save()
    }
  })
  .catch( err => {console.log("[Create Event] [Error] fetch to Base Station") ; structureRes.status = 'ERROR'})

  //Enviando estructura del response
  res.status(200).json(structureRes)

})

export default router
