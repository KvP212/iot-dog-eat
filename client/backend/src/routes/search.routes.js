import { Router } from 'express'
import fetch from 'node-fetch'
import Infodata from '../models/info_data'
import Hwpesodata from '../models/hw_peso_data'
import {URLBS} from '../globalV';
import moment from 'moment'
const router = Router()

//POST Search
router.post('/', async (req, res) =>{

  const {id, url, date, search} = req.body
  console.log("---SEARCH---")
  console.log("id: " + id)
  console.log("url: " + url)
  console.log("date: " + date)

  //Res structure
  const structureRes = {
    "id": "PT_CC8Project212",
    "url": URLBS,
    "date": moment().utcOffset(-360)._d,
    "search": {
      "id_hardware": search.id_hardware,
      "type": ""
    },
    "data": {}
  }

  const infodatas = await Infodata.find({idPT: "PT_CC8Project212"})

  if(search.id_hardware === "id01") structureRes.search.type = infodatas[0].hardware.id01.type
  if(search.id_hardware === "id02") structureRes.search.type = infodatas[0].hardware.id02.type
  if(search.id_hardware === "id03") structureRes.search.type = infodatas[0].hardware.id03.type

  const lastDateData = await Hwpesodata.find().sort({$natural:-1}).limit(1)

  if( lastDateData.length != 0){

    if( moment(search.finish_date).utcOffset(360)._d > lastDateData[0].date){
      await fetch( 'http://' + URLBS + '/search/saveSearch', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({
          id,
          url,
          date,
          search: {
            start_date: lastDateData[0].date.toISOString(),
            finish_date: moment(search.finish_date).utcOffset(360)._d
          }
        })
      })
      .then( resFetch => resFetch.json())
      .then( async (datos) => {

        for(let o of datos ){
          const saveData = new Hwpesodata({
            date: o.date,
            id01: o.id01,
            id02: o.id02,
            id03: o.id03
          })
          await saveData.save()
        }

      })
      .catch( err => console.log("Error fetch nH: " + err))

    }
  } else {

    await fetch( 'http://' + URLBS + '/search/saveSearch',{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify({
        id,
        url,
        date,
        search: {
          start_date: "2019-11-1 04:16:14.609Z",
          finish_date: "2019-11-3 02:30:55.206Z"
        }
      })
    })
    .then( resFetch => resFetch.json())
    .then( async (datos) => {

      for(let o of datos ){
        const saveData = new Hwpesodata({
          date: o.date,
          id01: o.id01,
          id02: o.id02,
          id03: o.id03
        })
        await saveData.save()
      }

    })
    .catch( err => console.log("Error fetch nH: " + err))

  }

  const searchdatas = await Hwpesodata.find(
    {
      "date": {
        $gt: moment(search.start_date).utcOffset(360)._d,
        $lt: moment(search.finish_date).utcOffset(360)._d
      }
    }
  )

  var data = "{"
  if(search.id_hardware === "id01"){

    for(var i = 0; i < searchdatas.length ; i++){
      data +=
        `"${searchdatas[i].date.toISOString()}":
          {
            "sensor": ${searchdatas[i].id01.sensor},
            "freq": ${searchdatas[i].id01.freq}
          }`

      if(i !== searchdatas.length - 1){
        data += ","
      }
    }

  } else if (search.id_hardware === "id02"){

    for(var i = 0; i < searchdatas.length ; i++){
      data +=
        `"${searchdatas[i].date.toISOString()}":
          {
            "status": ${searchdatas[i].id02.status},
            "text": "${searchdatas[i].id02.text}"
          }`

      if(i !== searchdatas.length - 1){
        data += ","
      }
    }

  } else if (search.id_hardware === "id03"){

    for(var i = 0; i < searchdatas.length ; i++){
      data +=
        `"${searchdatas[i].date.toISOString()}":
          {
            "status": ${searchdatas[i].id03.status},
            "text": "${searchdatas[i].id03.text}"
          }`

      if(i !== searchdatas.length - 1){
        data += ","
      }
    }

  }

  data += "}"
  structureRes.data = JSON.parse(data)
  res.status(200).json(structureRes)
})

router.post('/saveSearch', async (req, res) =>{

})

export default router
