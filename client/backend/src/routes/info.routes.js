import { Router } from 'express'
import fetch from 'node-fetch'
import Infodata from '../models/info_data'
import ReqInfodata from '../models/req_info_data'
import {URLBS} from '../globalV';
const router = Router()

//GET todos los Infos
router.get('/', async(req, res) => {
  const docsFounds = await Infodata.find()
  res.json(docsFounds)
})

//POST Info
router.post('/', async (req, res) =>{
  var resBSorBU = {}
  var infodatas = {}
  const {id, url, date, hardware} = req.body
  console.log("---INFO---")
  console.log("id: " + id)
  console.log("url: " + url)
  console.log("date: " + date)

  await Infodata.findOne({idPT: id})
    .then(resBackUp => {
      if(resBackUp) {console.log("Found: " + resBackUp)}
      else{
        console.log("Not Found in BackUp")
        console.log("Save and Go to Base Station to save")
        infodatas = new Infodata({
          idPT: id,
          url,
          hardware
        })
        infodatas.save()
      }

    })
    .catch(err => console.log("Error findeOne: " + err))

  if(Object.keys(infodatas).length != 0){
    await fetch( 'http://' + URLBS + '/info/newHardware', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify(infodatas)
    })
    .then( resFetch => resFetch.json())
    .then(datos => {
      resBSorBU = datos
    })
    .catch( err => console.log("Error fetch nH: " + err))
  }

  res.status(200).json(resBSorBU)

})


export default router
