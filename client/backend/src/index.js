import express, { json } from 'express';
import cors from 'cors'
import morgan from 'morgan';
import mongoose from './db/localDatabse';
import infoRoutes from './routes/info.routes'
import searchRoutes from './routes/search.routes'
import changeRoutes from './routes/change.routes'
import createRoutes from './routes/create.routes'
import updateRoutes from './routes/update.routes'
import deleteRoutes from './routes/delete.routes'

const app = express();

//Settings
app.set('port', process.env.PORT || 4040);

//middlewares
//morgan - peticiones navegador, cliente
app.use(morgan('dev'));
app.use(json());
app.use(cors())

//Routes
app.use('/info', infoRoutes)
app.use('/search', searchRoutes)
app.use('/change', changeRoutes)
app.use('/create', createRoutes)
app.use('/update', updateRoutes)
app.use('/delete', deleteRoutes)

//Starting the server
app.listen(app.get('port'), () => console.log(`Server BackUp port ${app.get('port')}`));

export default app
