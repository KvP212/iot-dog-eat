import mongoose, { Schema } from "mongoose"

const hw01 = new Schema({ sensor: Number, freq:Number}, { _id: false})
const hw02 = new Schema({ status: Boolean, text: String}, { _id: false})

const hwpesodataSchema = new Schema({
  date:{
    type: Date,
    default: Date.now
  },
  id01: hw01,
  id02: hw02,
  id03: hw02
})

export default mongoose.model('Hwpesodata', hwpesodataSchema)
