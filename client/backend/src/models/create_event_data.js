import mongoose, { Schema } from "mongoose"

// const datain = new Schema({ date: Date, status: String}, { _id: false})
const createeventdataSchema = new Schema({
  idFE:
    {
      type: String,
      required: true
    },

  url:
    {
      type: String
    },

  date:
    {
      type: Date,
      default: Date.now
    },

  create:
    {
      type: Object
    },

  idEvent:
    {
      type: String
    }
})

export default mongoose.model('Createdata', createeventdataSchema)
