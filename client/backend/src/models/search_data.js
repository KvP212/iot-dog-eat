import mongoose, { Schema } from "mongoose"

// const datain = new Schema({ date: Date, status: String}, { _id: false})
const searchdataSchema = new Schema({
  idPT:
    {
      type: String,
      required: true
    },

  url:
    {
      type: String
    },

  date:
    {
      type: Date,
      default: Date.now
    },

  search:
    {
      type: Object
    },

  data:
    {
      type: Object
    }
})

export default mongoose.model('Searchdata', searchdataSchema)
