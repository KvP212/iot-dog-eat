import mongoose, { Schema } from "mongoose"

const infodataSchema = new Schema({
  idPT:
    {
      type: String,
      required: true
    },

  url:
    {
      type: String
    },

  date:
    {
      type: Date,
      default: Date.now
    },

  hardware: {
    type: Object
  }
})

export default mongoose.model('Infodata', infodataSchema)
