import React, {useEffect} from 'react'
import { connect } from "react-redux";
import * as moment from 'moment'
import M from 'materialize-css'
import {URLFE, URLBACKUP} from '../../shared/js/globalV'
import UpdateEventModal from '../UpdateEventModal';

const ExecTab = ({info, events, handleUpdateEvents, handleIdEvent}) => {

  useEffect( () => {

  }, [events])

  const getEvents = () => {
    fetch( 'http://' + URLBACKUP + '/create')
    .then( resFetch => resFetch.json())
    .then(datos => {
      handleUpdateEvents(datos)
    })
    .catch( err => console.log("[ERROR][GET] fetch Events: " + err))
  }

  const handleDelet = id => {

    const reqSearch = {
      "id": "FE_CC8Project212",
      "url": URLFE,
      "date": moment().utc(true).toISOString(),
      "delete": {
        "id": id
      }
    }

    fetch('http://' + URLBACKUP + '/delete',{
      method: "POST",
      body: JSON.stringify(reqSearch),
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      }
    })
    .then( resFetch => resFetch.json())
    .then(datos => {
      M.toast({html: `${datos.status}`})
      if(datos.status === "OK"){
        getEvents()
      }
    })
    .catch( err => console.log("[ERROR][POST] fetch Delete: " + err))

  }

  return (
    <div className="container">

      <h2>Events Executed</h2>

      <ul className="collection">
        {
          events.map( eventElem => {
            var parrafo = ""
            var parrafoThen = ""
            var parrafoElse = ""
            var tagId =""

            if(eventElem.create.if.left.url !== undefined){
               tagId = info.find( infoElem => (infoElem.url === eventElem.create.if.left.url))
            }

            parrafo += `Si el valor del ${tagId.hardware[eventElem.create.if.left.id].tag} ${eventElem.create.if.condition} `

            if(eventElem.create.if.right.text !== undefined){
              parrafo += `${eventElem.create.if.right.text} `
            }

            if(eventElem.create.if.right.status !== undefined){
              parrafo += `${eventElem.create.if.right.status} `
            }

            if(eventElem.create.if.right.freq !== undefined){
              parrafo += `${eventElem.create.if.right.freq} `
            }

            if(eventElem.create.if.right.sensor !== undefined){
              parrafo += `${eventElem.create.if.right.sensor} `
            }

            parrafo += `en la plataforma ${tagId.idPT} `

            if(eventElem.create.then !== undefined){
              var tagIdThen = ""

              if(eventElem.create.then.url !== undefined){
                tagIdThen = info.find( infoElem => (infoElem.url === eventElem.create.then.url))
             }

              parrafoThen += `Entonces ${tagIdThen.hardware[eventElem.create.then.id].tag} en la plataforma ${tagIdThen.idPT} cambia `

              if(eventElem.create.then.freq !== undefined){
                parrafoThen += `la freq a ${eventElem.create.then.freq} ms `
              }

              if(eventElem.create.then.text !== undefined && eventElem.create.then.status !== undefined){
                parrafoThen += `el texto a ${eventElem.create.then.text} y el status a ${eventElem.create.then.status} `
              } else if(eventElem.create.then.text !== undefined){
                parrafoThen += `el texto a ${eventElem.create.then.text} `
              } else if(eventElem.create.then.status !== undefined){
                parrafoThen += `el status a ${eventElem.create.then.status} `
              }
            }


            if(eventElem.create.else !== undefined){

              var tagIdElse = ""

              if(eventElem.create.else.url !== undefined){
                tagIdElse = info.find( infoElem => (infoElem.url === eventElem.create.else.url))
             }

              parrafoElse += `Sino ${tagIdElse.hardware[eventElem.create.else.id].tag} en la plataforma ${tagIdElse.idPT} cambia `

              if(eventElem.create.else.freq !== undefined){
                parrafoElse += `la freq a ${eventElem.create.else.freq} ms `
              }

              if(eventElem.create.else.text !== undefined && eventElem.create.else.status !== undefined){
                parrafoElse += `el texto a ${eventElem.create.else.text} y el status a ${eventElem.create.else.status} `
              } else if(eventElem.create.else.text !== undefined){
                parrafoElse += `el texto a ${eventElem.create.else.text} `
              } else if(eventElem.create.else.status !== undefined){
                parrafoElse += `el status a ${eventElem.create.else.status} `
              }
            }

            return (
              <li className="collection-item" key={eventElem.idEvent}>
                <div className="row valign-wrapper">
                  <div className="col s10">
                      <h6>Evento creado {moment.utc(eventElem.date).format("MMM DD YYYY, h:mm:ss")}</h6>
                    <p>
                      {parrafo}
                      <br/>
                      {parrafoThen}
                      <br/>
                      {parrafoElse}
                    </p>
                  </div>

                  <div className="col s2">
                    <a className="btn-floating modal-trigger purple darken-4" href="#modal1"
                    onClick={ () => handleIdEvent(eventElem.idEvent)}
                    ><i className="material-icons">cached</i></a>
                    &nbsp;
                    <a className="btn-floating red" href="#!" onClick={ ()=> handleDelet(eventElem.idEvent) }><i className="material-icons">delete_forever</i></a>
                  </div>

                </div>
              </li>
            )
          })
        }
      </ul>

      <UpdateEventModal/>

    </div>

  )
}



const mapStateToProps = state =>({
  events: state.events,
  idEvent: state.idEvent,
  info: state.info
})

const mapDispatchToProps = dispatch =>({
  handleUpdateEvents(datos){
    dispatch({
      type: "UPDATEEVENTSARRAY",
      datos
    })
  },

  handleIdEvent(idEvent){
    dispatch({
      type: "SAVE_IDEVENT",
      idEvent
    })
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(ExecTab)
