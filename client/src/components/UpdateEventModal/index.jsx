import React,{useState, useEffect} from 'react'
import { connect } from "react-redux";
import M from 'materialize-css'
import {URLFE, URLBACKUP} from '../../shared/js/globalV'
import moment from 'moment'

const OptionIdPT = ({info}) => {
  const option = info.map( infoElem => (
    <option key={infoElem.idPT} value={infoElem.idPT}> {infoElem.idPT} </option>
  ))
  return option
}

const OptionHw = ({info, idPT}) => {
  const option = info.map( infoElem => {

    var optionsHardware;

    if(infoElem.idPT === idPT) {

      optionsHardware = Object.keys(infoElem.hardware).map( key => {
        return (
          <option key={key} value={key}> {infoElem.hardware[key].tag} </option>
        )
      })
    }

    return optionsHardware
  })

  return option
}

const UpdateEventModal = ({info, events, idEvent, eventObj, isUpdateCreate, handleIdEvent, handleIsUpdate, handleUpdateEvents}) => {

  const [valueIdPT, setValueIdPT] = useState("")
  const [valueHwId, setValueHwId] = useState("")
  const [valueFreqIF, setValueFreqIF] = useState(0)
  const [valueCondIF, setValueCondIF] = useState("")
  const [valueTypeHwId, setValueTypeHwId] = useState("")
  const [typeRight, setTypeRight] = useState("")
  const [valueRight, setValueRight] = useState()

  const [valueIdPTThen, setValueIdPTThen] = useState("")
  const [valueHwIdThen, setValueHwIdThen] = useState("")
  const [valueTypeHwIdThen, setValueTypeHwIdThen] = useState("")
  const [statusThen, setStatusThen] = useState()
  const [textThen, setTextThen] = useState("")
  const [freqThen, setFreqThen] = useState(0)

  const [valueIdPTElse, setValueIdPTElse] = useState("")
  const [valueHwIdElse, setValueHwIdElse] = useState("")
  const [valueTypeHwIdElse, setValueTypeHwIdElse] = useState("")
  const [statusElse, setStatusElse] = useState()
  const [textElse, setTextElse] = useState("")
  const [freqElse, setFreqElse] = useState(0)

  const [urlLeft, setUrlLeft] = useState("")
  const [urlThen, setUrlThen] = useState("")
  const [urlElse, setUrlElse] = useState("")

  const handleUpdateEvent = () => {

    const reqUpdate = {
      "id": "FE_CC8Project212",
      "url": URLFE,
      "date": moment().utc(true).toISOString(),
      "update":{
        "id": eventObj.idEvent
      }
    }

    var changesIF = false
    var changesThen = false
    var changesElse = false

    if( eventObj.create.if.left.url !== urlLeft) changesIF = true
    if( eventObj.create.if.left.id !== valueHwId) changesIF = true
    if( eventObj.create.if.left.freq !== undefined && eventObj.create.if.left.freq !== valueFreqIF) changesIF = true
    if( eventObj.create.if.condition !== valueCondIF) changesIF = true
    if( eventObj.create.if.right.sensor !== undefined && eventObj.create.if.right.sensor !== valueRight) changesIF = true
    if( eventObj.create.if.right.status !== undefined && eventObj.create.if.right.status !== valueRight) changesIF = true
    if( eventObj.create.if.right.freq !== undefined && eventObj.create.if.right.freq !== valueRight) changesIF = true
    if( eventObj.create.if.right.text !== undefined && eventObj.create.if.right.text !== valueRight) changesIF = true

    if(changesIF){

      var objectIF = `
        {
          "if": {
            "left": {
              "url": "${urlLeft}",
              "id": "${valueHwId}"
      `
      if(valueFreqIF !== 0){
        objectIF += `, "freq": ${valueFreqIF}},`
      } else {
        objectIF += `},`
      }

      objectIF += `"condition": "${valueCondIF}",`
      objectIF += `"right": {`

      switch (typeRight) {
        case "sensor":
          objectIF += `"sensor": ${valueRight}}`
          break

        case "status":
            objectIF += `"status": ${valueRight}}`
            break

        case "freq":
            objectIF += `"freq": ${valueRight}}`
            break

        case "text":
            objectIF += `"text": "${valueRight}"}`
            break

        default:
            objectIF += `}`
          break
      }

      objectIF += `}}`
      Object.assign( reqUpdate.update, JSON.parse(objectIF) )
    }

    if(eventObj.create.then !== undefined) {
      if( eventObj.create.then.url !== urlThen ) changesThen = true
      if( eventObj.create.then.id !== valueHwIdThen ) changesThen = true
      if( eventObj.create.then.freq !== undefined && eventObj.create.then.freq !== freqThen ) changesThen = true
      if( eventObj.create.then.status !== undefined && eventObj.create.then.status !== statusThen ) changesThen = true
      if( eventObj.create.then.text !== undefined && eventObj.create.then.text !== textThen ) changesThen = true
    }

    if(changesThen){

      var objectThen = `
        {
          "then": {
            "url": "${urlThen}",
            "id": "${valueHwIdThen}",
      `

      if( freqThen !== 0) {
        objectThen += `"freq": ${freqThen}`
      }

      if( statusThen !== undefined && textThen !== ""){
        objectThen += `"status": ${statusThen}, "text": "${textThen}"`
      } else if ( statusThen !== undefined ){
        objectThen += `"status": ${statusThen}`
      } else if ( textThen !== "" ){
        objectThen += `"text": "${textThen}"`
      }

      objectThen += `}}`
      Object.assign( reqUpdate.update, JSON.parse(objectThen) )
    }

    if(eventObj.create.else !== undefined) {
      if( eventObj.create.else.url !== urlElse ) changesElse = true
      if( eventObj.create.else.id !== valueHwIdElse ) changesElse = true
      if( eventObj.create.else.freq !== undefined && eventObj.create.else.freq !== freqElse ) changesElse = true
      if( eventObj.create.else.status !== undefined && eventObj.create.else.status !== statusElse ) changesElse = true
      if( eventObj.create.else.text !== undefined && eventObj.create.else.text !== textElse ) changesElse = true
    }

    if(changesElse){

      var objectElse = `
        {
          "else": {
            "url": "${urlElse}",
            "id": "${valueHwIdElse}",
      `

      if( freqElse !== 0) {
        objectElse += `"freq": ${freqElse}`
      }

      if( statusElse !== undefined && textElse !== ""){
        objectElse += `"status": ${statusElse}, "text": "${textElse}"`
      } else if ( statusElse !== undefined ){
        objectElse += `"status": ${statusElse}`
      } else if ( textElse !== "" ){
        objectElse += `"text": "${textElse}"`
      }

      objectElse += `}}`
      Object.assign( reqUpdate.update, JSON.parse(objectElse) )
    }

    //SEND UPDATE TO BS
    fetch('http://' + URLBACKUP + '/update',{
      method: "POST",
      body: JSON.stringify(reqUpdate),
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      }
    })
    .then( resFetch => resFetch.json())
    .then(datos => {
        M.toast({html: `${datos.status}`})

        fetch( 'http://' + URLBACKUP + '/create')
        .then( resFetch => resFetch.json())
        .then(datos => {
          handleUpdateEvents(datos)
        })
        .catch( err => console.log("[ERROR][GET] fetch Events: " + err))

    })
    .catch( err => console.log("[ERROR][POST] fetch Event to anywhere: " + err))

  }

  useEffect(() => {

    if(idEvent !== "" && !isUpdateCreate ){

      if(Object.entries(eventObj).length !== 0){

        const infoBS = info.find( infoElem => (infoElem.url === eventObj.create.if.left.url))
        if( infoBS.idPT !== undefined ) {setValueIdPT( infoBS.idPT )}
        setValueHwId( eventObj.create.if.left.id )

        if (eventObj.create.if.left.freq !== undefined){
          setValueFreqIF(eventObj.create.if.left.freq)
        }

        setValueCondIF(eventObj.create.if.condition)

        if(eventObj.create.if.right.text !== undefined){
          setTypeRight("text")
          setValueRight(eventObj.create.if.right.text)

        } else if(eventObj.create.if.right.status !== undefined){
          setTypeRight("status")
          setValueRight(eventObj.create.if.right.status)

        } else if(eventObj.create.if.right.freq !== undefined){
          setTypeRight("freq")
          setValueRight(eventObj.create.if.right.freq)

        } else if(eventObj.create.if.right.sensor !== undefined){
          setTypeRight("sensor")
          setValueRight(eventObj.create.if.right.sensor)
        }

        if(eventObj.create.then !== undefined){

          const infoBSThen = info.find( infoElem => (infoElem.url === eventObj.create.then.url))

          setValueIdPTThen( infoBSThen.idPT )
          setValueHwIdThen( eventObj.create.then.id )

          if(eventObj.create.then.freq !== undefined){
            setValueTypeHwIdThen("input")
            setFreqThen(eventObj.create.then.freq)
          }

          if(eventObj.create.then.text !== undefined
            && eventObj.create.then.status !== undefined){
            setValueTypeHwIdThen("output")
            setTextThen(eventObj.create.then.text)
            setStatusThen(eventObj.create.then.status)

          } else if(eventObj.create.then.text !== undefined){
            setValueTypeHwIdThen("output")
            setTextThen(eventObj.create.then.text)

          } else if(eventObj.create.then.status !== undefined){
            setValueTypeHwIdThen("output")
            setStatusThen(eventObj.create.then.status)
          }

        }

        if(eventObj.create.else !== undefined){

          const infoBSElse = info.find( infoElem => (infoElem.url === eventObj.create.else.url))

          setValueIdPTElse( infoBSElse.idPT )
          setValueHwIdElse( eventObj.create.else.id )

          if(eventObj.create.else.freq !== undefined){
            setValueTypeHwIdElse("input")
            setFreqElse(eventObj.create.else.freq)
          }

          if(eventObj.create.else.text !== undefined
            && eventObj.create.else.status !== undefined){
            setValueTypeHwIdElse("output")
            setTextElse(eventObj.create.else.text)
            setStatusElse(eventObj.create.else.status)

          } else if(eventObj.create.else.text !== undefined){
            setValueTypeHwIdElse("output")
            setTextElse(eventObj.create.else.text)

          } else if(eventObj.create.else.status !== undefined){
            setValueTypeHwIdElse("output")
            setStatusElse(eventObj.create.else.status)
          }

        }

      }

      handleIsUpdate()

    }

    var elems = document.querySelectorAll('select')
    M.FormSelect.init(elems)

    if(valueHwId !== ""){
      info.map( infoElem => {

        if(infoElem.idPT === valueIdPT){
          setUrlLeft(infoElem.url)
          Object.keys(infoElem.hardware).map(
            key => {
              if(key === valueHwId){
                setValueTypeHwId(infoElem.hardware[key].type)
              }
              return null
            }
          )
        }
        return null
      })
    }

    if(valueHwIdThen !== ""){
      info.map( infoElem => {

        if(infoElem.idPT === valueIdPTThen){
          setUrlThen(infoElem.url)
          Object.keys(infoElem.hardware).map(
            key => {
              if(key === valueHwIdThen){
                setValueTypeHwIdThen(infoElem.hardware[key].type)
              }
              return null
            }
          )
        }
        return null
      })
    }

    if(valueHwIdElse !== ""){
      info.map( infoElem => {

        if(infoElem.idPT === valueIdPTElse){
          setUrlElse(infoElem.url)
          Object.keys(infoElem.hardware).map(
            key => {
              if(key === valueHwIdElse){
                setValueTypeHwIdElse(infoElem.hardware[key].type)
              }
              return null
            }
          )
        }
        return null
      })
    }

  }, [info, events, idEvent, eventObj, valueIdPT, valueHwId, valueTypeHwId, typeRight, valueRight, valueIdPTThen, valueHwIdThen, valueTypeHwIdThen, textThen, valueIdPTElse, valueHwIdElse, valueTypeHwIdElse, textElse, isUpdateCreate, handleIsUpdate ])

  return (
    <div id="modal1" className="modal">
      <div className="modal-content">
        <h4>Actualizar Evento</h4>
        <div className="row">
          {/* if */}
          <div className="col s12">

            <h5>IF</h5>

            <div className="input-field col s4">

              <select className="browser-default" value={valueIdPT} onChange={ (e) => {
                setValueIdPT(e.target.value)

                setValueHwId("DEFAULT")
                setValueFreqIF(0)

                setValueCondIF("DEFAULT")
                setValueTypeHwId("")
                setTypeRight("DEFAULT")
                setValueRight()
                }}>
                <option value="DEFAULT" disabled>Choose </option>
                <OptionIdPT info={info}/>
              </select>
              <label className="active">Base Station</label>
            </div>

            <div className="input-field col s4">
              <select className="browser-default" value={valueHwId} onChange={ (e) => {
                setValueHwId(e.target.value)

                setValueFreqIF(0)
                setValueCondIF("DEFAULT")
                setValueTypeHwId("")
                setTypeRight("DEFAULT")
                setValueRight()
              }}>
                <option value="DEFAULT" disabled>Choose</option>
                <OptionHw info={info} idPT={valueIdPT}/>
              </select>
              <label className="active">Hardware ID</label>
            </div>

            <div className="input-field col s2">
              {
                valueIdPT === "PT_CC8Project212" || valueIdPT === ""?
                (<input id="freq_if_modal" type="number" className="validate" value={valueFreqIF} disabled/>)
                :
                (<input id="freq_if_modal" type="number" className="validate" value={valueFreqIF} onChange={(e) => {
                  setValueFreqIF(e.target.value)
                }}/>)
              }
              <label className="active"  htmlFor="freq_if_modal">Freq</label>
            </div>

          </div>

          <div className="col s12">

            <div className="input-field col s2">
              {(()=> {
                switch (valueTypeHwId) {
                  case "input":
                    return (
                      <select className="browser-default" value={valueCondIF} onChange={ (e) => {
                        setValueCondIF(e.target.value)}}>
                        <option value="DEFAULT" disabled selected>Choose</option>
                        <option value="="> &#61; </option>
                        <option value="!="> &#33;&#61; </option>
                        <option value="<"> &#60; </option>
                        <option value="<="> &#60;&#61; </option>
                        <option value=">"> &#62; </option>
                        <option value=">="> &#62;&#61; </option>
                      </select>
                    )
                  case "output":
                    return (
                      <select className="browser-default" value={valueCondIF} onChange={ (e) => {
                        setValueCondIF(e.target.value)}}>
                        <option value="DEFAULT" disabled selected>Choose</option>
                        <option value="="> &#61; </option>
                        <option value="!="> &#33;&#61; </option>
                      </select>
                    )
                  default:
                    return(
                      <select className="browser-default" defaultValue="DEFAULT">
                        <option value="DEFAULT" disabled>Choose</option>
                      </select>
                    )
                }
              })()}
              <label className="active">Condition</label>
            </div>

            <div className="input-field col s2">
              {(()=> {
                switch (valueTypeHwId) {
                  case "input":
                    return (
                      <select className="browser-default" value={typeRight} onChange={ (e) => {
                        setTypeRight(e.target.value)
                        setValueRight()
                      }}>
                        <option value="DEFAULT" disabled selected>Choose</option>
                        <option value="sensor"> sensor </option>
                        <option value="freq"> freq </option>
                      </select>
                    )
                  case "output":
                    return (
                      <select className="browser-default" value={typeRight} onChange={ (e) => {
                        setTypeRight(e.target.value)
                        setValueRight()
                      }}>
                        <option value="DEFAULT" disabled selected>Choose</option>
                        <option value="status"> status </option>
                        <option value="text"> text </option>
                      </select>
                    )
                  default:
                    return(
                      <select className="browser-default" defaultValue="DEFAULT">
                        <option value="DEFAULT" disabled>Choose</option>
                      </select>
                    )
                }
              })()}
            </div>

            {(()=> {
              switch (typeRight) {
                case "sensor":
                case "freq":
                  return (
                    <div className="input-field col s3">
                      <input id="freq_if_right" type="number" className="validate" value={valueRight} onChange={(e) => {
                        setValueRight(e.target.valueAsNumber)
                      }}/>
                    </div>
                    )
                case "text":
                  return (
                    <div className="input-field col s3">
                      <input id="text_if_right" type="text" className="validate" value={valueRight} onChange={(e) => {
                        setValueRight(e.target.value)
                      }}/>
                      <label className="active"  htmlFor="text_if_right">Text</label>
                    </div>
                  )
                case "status":
                  return (
                    <div className="input-field col s3">
                      <input id="status_if_right" type="text"
                      className="validate"
                      defaultValue={valueRight}
                      placeholder="true or false"
                      onChange={(e) => {
                        setValueRight(e.target.value === "true"? true : false)
                      }}/>
                      <label className="active" htmlFor="status_if_right">Status</label>
                    </div>
                  )
                default:
                  return null
              }
            })()}

          </div>

          {/* Then */}
          <div className="col s12">
            <h5>Then</h5>

            <div className="input-field col s4">
              <select className="browser-default" value={valueIdPTThen} onChange={ (e) => {
                setValueIdPTThen(e.target.value)

                setValueHwIdThen("DEFAULT")
                setValueTypeHwIdThen("")
                setStatusThen()
                setTextThen("")
                setFreqThen(0)
              }}>
                <option value="DEFAULT" disabled>Choose </option>
                <OptionIdPT info={info}/>
              </select>
              <label className="active">Base Station</label>
            </div>

            <div className="input-field col s2">
              <select className="browser-default" value={valueHwIdThen} onChange={ (e) => {
                setValueHwIdThen(e.target.value)

                setStatusThen()
                setTextThen("")
                setFreqThen(0)
              }}>
                <option value="DEFAULT" disabled>Choose</option>
                <OptionHw info={info} idPT={valueIdPTThen}/>
              </select>
              <label className="active">Hardware ID</label>
            </div>

            {(() => {
              switch (valueTypeHwIdThen) {
                case "input":
                  return (
                    <div className="col s4">
                      <div className="input-field col s12">
                        <input id="freq_then_modal" type="number" value={freqThen} className="validate" onChange={(e) => {
                        setFreqThen(e.target.valueAsNumber)
                        }}/>
                        <label className="active" htmlFor="freq_then_modal">Freq</label>
                      </div>
                    </div>
                  )

                case "output":
                  return (
                    <div className="col s6">

                      <div className="input-field col s6">
                        <input id="text_then_modal" type="text" className="validate" value={textThen} onChange={(e) => {
                        setTextThen(e.target.value)
                        }}/>
                        <label className="active"  htmlFor="text_then_modal">text</label>
                      </div>

                      <div className="input-field col s6">
                        <input id="status_then_modal" type="text"
                        className="validate"
                        placeholder="true or false"
                        defaultValue={statusThen} onChange={(e) => {
                          setStatusThen(e.target.value === "true"? true : false)
                        }}/>
                        <label className="active" htmlFor="status_then_modal">Status</label>
                      </div>

                    </div>
                  )
                default:
                  return null
              }
            })()}

          </div>

          {/* Else */}
          <div className="col s12">

          <h5>Else</h5>

          <div className="input-field col s4">
            <select className="browser-default" value={valueIdPTElse} onChange={ (e) => {
              setValueIdPTElse(e.target.value)

              setValueHwIdElse("DEFAULT")
              setValueTypeHwIdElse("")
              setStatusElse()
              setTextElse("")
              setFreqElse(0)
            }}>
              <option value="DEFAULT" disabled>Choose </option>
              <OptionIdPT info={info}/>
            </select>
            <label className="active">Base Station</label>
          </div>

          <div className="input-field col s2">
            <select className="browser-default" value={valueHwIdElse} onChange={ (e) => {
                setValueHwIdElse(e.target.value)

                setStatusElse()
                setTextElse("")
                setFreqElse(0)
            }}>
              <option value="DEFAULT" disabled>Choose</option>
              <OptionHw info={info} idPT={valueIdPTElse}/>
            </select>
            <label className="active">Hardware ID</label>
          </div>

          {(() => {
            switch (valueTypeHwIdElse) {
              case "input":
                return (
                  <div className="col s4">
                    <div className="input-field col s12">
                      <input id="freq_Else_modal" type="number" value={freqElse} className="validate" onChange={(e) => {
                      setFreqElse(e.target.valueAsNumber)
                      }}/>
                      <label className="active" htmlFor="freq_Else_modal">Freq</label>
                    </div>
                  </div>
                )

              case "output":
                return (
                  <div className="col s6">

                    <div className="input-field col s6">
                      <input id="text_Else_modal" type="text" className="validate" value={textElse} onChange={(e) => {
                      setTextElse(e.target.value)
                      }}/>
                      <label className="active"  htmlFor="text_Else_modal">text</label>
                    </div>

                    <div className="input-field col s6">
                      <input id="status_then_modal"
                      type="text"
                      defaultValue={statusElse}
                      className="validate"
                      placeholder="true or false"
                      onChange={(e) => {
                        setStatusElse(e.target.value === "true"? true : false)
                      }}/>
                      <label className="active" htmlFor="status_then_modal">Status</label>
                    </div>

                  </div>
                )

              default:
                return null
            }
          })()}

          </div>
        </div>

      </div>
      <div className="modal-footer">
        <a href="#!" className="modal-close waves-effect waves-green btn-flat" onClick={(e) =>{
          handleIdEvent("")
          handleIsUpdate()

          setValueIdPT("")
          setValueHwId("")
          setValueFreqIF(0)
          setValueCondIF("")
          setValueTypeHwId("")
          setTypeRight("")
          setValueRight()

          setValueIdPTElse("")
          setValueHwIdThen("")
          setValueTypeHwIdThen("")
          setStatusThen()
          setTextThen("")
          setFreqThen(0)

          setValueIdPTElse("")
          setValueHwIdElse("")
          setValueTypeHwIdElse("")
          setStatusElse()
          setTextElse("")
          setFreqElse(0)

        }}>Cancel</a>
        <a href="#!" className="modal-close waves-effect waves-green btn-flat" onClick={ () => handleUpdateEvent()}>Agree</a>
      </div>
    </div>
  )
}

const mapStateToProps = state =>({
  info: state.info,
  events: state.events,
  idEvent: state.idEvent,
  eventObj: state.eventObj,
  isUpdateCreate: state.isUpdateCreate
})

const mapDispatchToProps = dispatch =>({
  handleIdEvent(idEvent){
    dispatch({
      type: "SAVE_IDEVENT",
      idEvent
    })
  },

  handleIsUpdate(){
    dispatch({
      type: "ISUPDATE"
    })
  },

  handleUpdateEvents(datos){
    dispatch({
      type: "UPDATEEVENTSARRAY",
      datos
    })
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(UpdateEventModal)
