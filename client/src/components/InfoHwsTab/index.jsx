import React from 'react'
import { connect } from "react-redux";
import moment from 'moment'

function TabsHs(params) {

  const tabTabs = Object.keys(params.hardware).map( key =>
      <li className="collection-item avatar" key={key}>
        <i className="material-icons circle purple darken-4">extension</i>
        <span className="title purple-text text-darken-4"><strong>ID: {key}</strong></span>
        <p>&#8226; {params.hardware[key].tag}
          <br/>
          &#8226; {params.hardware[key].type}
        </p>
      </li>
  )
  return tabTabs
}

const InfoHwsTab = ({info}) => (
  <div className="row">
    {
      info.map( infoElem => (
      <div className="col s12" key={infoElem._id}>
        <div className="card hoverable">

          <div className="card-content">
            <span className="card-title center-align">{infoElem.idPT}</span>
            <p className="center-align"> <strong className="purple-text text-darken-4">URL: </strong>{infoElem.url}</p>
            <p className="center-align"> <strong className="purple-text text-darken-4">Fecha: </strong>{moment(infoElem.date).format('DD-MM-YYYY LTS')}</p>
            <ul className="collection">
              <TabsHs hardware={infoElem.hardware}/>
            </ul>
          </div>

        </div>
      </div>
      ))
    }
  </div>
)

const mapStateToProps = state =>({
  info: state.info
})

const mapDispatchToProps = dispatch =>({

})

export default connect(mapStateToProps, mapDispatchToProps)(InfoHwsTab)
