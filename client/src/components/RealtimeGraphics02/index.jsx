import React, {useState, useEffect} from 'react'
import { connect } from "react-redux"
import {Chart} from 'react-google-charts'

const RealtimeGraphics02 = ({graphicRT02}) => {

  const [dataGraphic1, setDataGraphic1] = useState([])
  const [dataGraphic2, setDataGraphic2] = useState([])
  const [options1, setOptions1] = useState({})
  const [options2, setOptions2] = useState({})
  const [tipoGraphic, setTipoGraphic] = useState("AreaChart")

  useEffect( () => {

    if(graphicRT02.length !== 0){
      if(graphicRT02.search.type === "input"){
        const columns = [
          {type:'date', label:'Instante'},
          {type:'number', label:'sensor'}
        ]

        const columns2 = [
          {type:'date', label:'Instante'},
          {type:'number', label:'Freq'}
        ]

        let rows = []
        let rows2 = []
        for( var row in graphicRT02.data){

          rows.push( [new Date(row) , graphicRT02.data[row].sensor] )
          rows2.push( [new Date(row) , graphicRT02.data[row].freq] )
        }

        const options = {
          hAxis: {
            title: 'Time',
          },
          vAxis:{
            title: 'Sensor',
            minValue: 0
          },
          legend: 'none'
        }

        const options2 = {
          hAxis: {
            title: 'Time',
          },
          vAxis:{
            title: 'Frequence',
            minValue: 0
          },
          legend: 'none'
        }

        setOptions1(options)
        setDataGraphic1([columns, ...rows])

        setTipoGraphic("AreaChart")
        setOptions2(options2)
        setDataGraphic2([columns2, ...rows2])

      } else {

        const columns = [
          {type:'date', label:'Instante'},
          {type:'number', label:'status'}
        ]

        const columns2 = [
          {type:'string', label:'Text'},
          {type:'number', label:'Cantidad'},
          { role: 'style' }
        ]

        let rows = []
        let rows2 = []
        let textRows = []
        for( var row2 in graphicRT02.data){

          rows.push([
            new Date(row2),
            (graphicRT02.data[row2].status === true)? 1:0
          ])

          rows2.push(graphicRT02.data[row2].text)
        }

        const mySet = new Set(rows2)
        for(var item of mySet) {
          var count = 0
          var colorChart = ""
          for(var item2 in graphicRT02.data){
            if( graphicRT02.data[item2].text === item){
              count++
            }
          }

          if(item === "low"){
            colorChart = "#ffa726"
          } else if (item === "medium"){
            colorChart = "#ff6d00"
          } else if (item === "high"){
            colorChart = "#e65100"
          }

          textRows.push([item, count, colorChart])
        }

        const options = {
          hAxis: {
            title: 'Time'
          },
          vAxis:{
            title: 'Status',
            minValue: 0
          },
          colors: ['#4a148c'],
          legend: 'none'
        }

        const options2 = {
          hAxis: {
            title: 'Count'
          },
          vAxis:{
            title: 'Text'
          },
          legend: 'none'
        }


        setOptions1(options)
        setDataGraphic1([columns, ...rows])

        setTipoGraphic("BarChart")
        setOptions2(options2)
        setDataGraphic2([columns2, ...textRows])

      }
    }

  }, [graphicRT02])

  return (
    <div className="col s12">
      {
        graphicRT02.length !== 0 ?
        (
          <div className="row">
            <h6>Motor</h6>
            <div className="col s12 m6">
              <Chart
                chartType="SteppedAreaChart"
                data={dataGraphic1}
                width="100%"
                height="100%"
                options={options1}
                loader={
                  <div className="progress">
                    <div className="indeterminate"></div>
                  </div>
                }
              />
            </div>

            <div className="col s12 m6">
              <Chart
                chartType={tipoGraphic}
                data={dataGraphic2}
                width="100%"
                height="100%"
                options={options2}
                loader={
                  <div className="progress">
                    <div className="indeterminate"></div>
                  </div>
                }
              />
            </div>

          </div>
        )
        :
        (
          null
        )
      }
    </div>
  )
}

const mapStateToProps = state =>({
  graphicRT02: state.graphicRT02
})

const mapDispatchToProps = dispatch =>({})

export default connect(mapStateToProps, mapDispatchToProps)(RealtimeGraphics02)
