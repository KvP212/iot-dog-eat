import React, {useState, useEffect} from 'react'
import { connect } from "react-redux";
import * as moment from 'moment'
import M from 'materialize-css'
import {URLFE} from '../../shared/js/globalV'

const OptionIdPT = ({info}) => {
  const option = info.map( infoElem => (
    <option key={infoElem.idPT} value={infoElem.idPT}> {infoElem.idPT} </option>
  ))

  return option
}

const OptionHw = ({info, idPT}) => {
  const option = info.map( infoElem => {

    var optionsHardware;

    if(infoElem.idPT === idPT) {

      optionsHardware = Object.keys(infoElem.hardware).map( key => {
        return (
          <option key={key} value={key}> {infoElem.hardware[key].tag} </option>
        )
      })
    }

    return optionsHardware
  })

  return option
}

const ChangeDirecto = ({info}) => {

  const [valueIdPTElse, setValueIdPTElse] = useState("DEFAULT")
  const [valueHwIdElse, setValueHwIdElse] = useState("")
  const [selectHwIdElse, setSelectHwIdElse] = useState(true)
  const [valueTypeHwIdElse, setValueTypeHwIdElse] = useState("")
  const [statusElse, setStatusElse] = useState()
  const [textElse, setTextElse] = useState("")
  const [freqElse, setFreqElse] = useState(0)
  const [urlElse, setUrlElse] = useState("")

  const changeHw = () => {
    var structureChange = {
      "id": "PT_CC8Project212",
      "url": URLFE,
      "date": moment().utc(true).toISOString(),
      "change": {}
    }

    var structureId = `{"${valueHwIdElse}":{`

    if(freqElse !== 0){
      structureId += `"freq": ${freqElse}`
    }

    if( textElse !== "" && statusElse !== undefined){
      structureId += `"status": ${statusElse}, "text": "${textElse}"`
    } else if ( textElse !== "" ){
      structureId += `"text": "${textElse}"`
    } else if ( statusElse !== undefined ){
      structureId += `"status": ${statusElse}`
    }

    structureId += `}}`
    Object.assign(structureChange.change, JSON.parse(structureId))

    fetch(
      `http://${urlElse}/change`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(structureChange)
      })
    .then( resFetch => resFetch.json())
    .then( datos => {

      M.toast({html: `Change ${datos.status}`})
      setValueIdPTElse("DEFAULT")
      setSelectHwIdElse(true)
      setValueTypeHwIdElse("")
      setValueHwIdElse("")

      setStatusElse()
      setTextElse("")
      setFreqElse(0)
    })
    .catch( err => console.log("[CHANGE DIRECTO][ERROR]: Fetch Change"))
  }

  useEffect( () => {
    var elems = document.querySelectorAll('select')
    M.FormSelect.init(elems)

    if(valueHwIdElse !== ""){
      info.map( infoElem => {

        if(infoElem.idPT === valueIdPTElse){
          setUrlElse(infoElem.url)
          Object.keys(infoElem.hardware).map(
            key => {
              if(key === valueHwIdElse){
                setValueTypeHwIdElse(infoElem.hardware[key].type)
              }
              return null
            }
          )
        }
        return null
      })
    }
  },[info, valueIdPTElse, valueHwIdElse, valueTypeHwIdElse, urlElse])

  return(
    <div className="row">

      <h5>Change</h5>
      <div className="col s12">

        <div className="input-field col s6 m3">
          {
            <select className="browser-default" value={valueIdPTElse} onChange={ (e) => {
              setValueIdPTElse(e.target.value)
              setSelectHwIdElse(false)
              setValueHwIdElse("")

              setStatusElse()
              setTextElse("")
              setFreqElse(0)
            }}>
              <option value="DEFAULT" disabled>Choose </option>
              <OptionIdPT info={info}/>
            </select>
          }
          <label className="active">Base Station</label>
        </div>

        <div className="input-field col s6 m3">
          {
            valueIdPTElse === "DEFAULT" ?
            (
              <select className="browser-default" defaultValue="DEFAULT" disabled>
                <option value="DEFAULT" disabled></option>
              </select>
            )
            :
            (
              <select className="browser-default" defaultValue="DEFAULT" onChange={ (e) => {
                  setSelectHwIdElse(true)
                  setValueHwIdElse(e.target.value)
                  setStatusElse()
                  setTextElse("")
                  setFreqElse(0)
              }}>
                {
                  selectHwIdElse? (<option value="DEFAULT" disabled>Choose</option>) : (<option value="DEFAULT" disabled selected>Choose</option>)
                }
                <OptionHw info={info} idPT={valueIdPTElse}/>
              </select>
            )
          }
          <label className="active">{ "Hardware Tag"}</label>
        </div>

        {(() => {
          switch (valueTypeHwIdElse) {
            case "input":
              return (
                <div className="col s4 m2">
                  <div className="input-field col s12">
                    <input id="freq_Else" type="number" className="validate" value={freqElse} onChange={(e) => {
                    setFreqElse(e.target.valueAsNumber)
                    }}/>
                    <label className="active" htmlFor="freq_Else">Freq</label>
                  </div>
                </div>
              )

            case "output":
              return (
                <div className="col s10 m4">

                  <div className="input-field col s6">
                    <input id="text_Else" type="text" className="validate" value={textElse} onChange={(e) => {
                    setTextElse(e.target.value)
                    }}/>
                    <label htmlFor="text_Else">text</label>
                  </div>

                  <div className="input-field col s6">
                    <input id="status_then" type="text"
                    className="validate"
                    value={(statusElse === undefined)? "": null}
                    placeholder="true or false"
                    onChange={(e) => {
                      setStatusElse(e.target.value === "true"? true : false)
                    }}/>
                    <label className="active" htmlFor="status_then">Status</label>
                  </div>

                </div>
              )

            default:
              return null
          }
        })()}

        <div className="col s12 m2">
          <a
          className={"waves-effect waves-light btn purple darken-4 " +
            ((freqElse !== 0 || textElse !== "" || statusElse !== undefined)? "": "disabled")}
          href="#!"
          onClick={ changeHw }>change</a>
        </div>

      </div>
    </div>
  )
}

const mapStateToProps = state =>({
  info: state.info
})

const mapDispatchToProps = dispatch =>({
  handleUpdateEvents(datos){
    dispatch({
      type: "UPDATEEVENTSARRAY",
      datos
    })
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(ChangeDirecto)
