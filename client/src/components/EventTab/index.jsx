import React, {useState, useEffect} from 'react'
import { connect } from "react-redux";
import * as moment from 'moment'
import M from 'materialize-css'
import {URLFE, URLBACKUP, URLBS} from '../../shared/js/globalV'

const OptionIdPT = ({info}) => {
  const option = info.map( infoElem => (
    <option key={infoElem.idPT} value={infoElem.idPT}> {infoElem.idPT} </option>
  ))

  return option
}

const OptionHw = ({info, idPT}) => {
  const option = info.map( infoElem => {

    var optionsHardware;

    if(infoElem.idPT === idPT) {

      optionsHardware = Object.keys(infoElem.hardware).map( key => {
        return (
          <option key={key} value={key}> {infoElem.hardware[key].tag} </option>
        )
      })
    }

    return optionsHardware
  })

  return option
}

const EventTab = ({info, handleUpdateEvents}) => {

  const [valueIdPT, setValueIdPT] = useState("DEFAULT")
  const [valueHwId, setValueHwId] = useState("")
  const [selectHwId, setSelectHwId] = useState(true)
  const [valueFreqIF, setValueFreqIF] = useState(0)
  const [valueCondIF, setValueCondIF] = useState("")
  const [valueTypeHwId, setValueTypeHwId] = useState("")
  const [typeRight, setTypeRight] = useState("")
  const [valueRight, setValueRight] = useState()

  const [valueIdPTThen, setValueIdPTThen] = useState("DEFAULT")
  const [valueHwIdThen, setValueHwIdThen] = useState("")
  const [selectHwIdThen, setSelectHwIdThen] = useState(true)
  const [valueTypeHwIdThen, setValueTypeHwIdThen] = useState("")
  const [statusThen, setStatusThen] = useState()
  const [textThen, setTextThen] = useState("")
  const [freqThen, setFreqThen] = useState(0)

  const [valueIdPTElse, setValueIdPTElse] = useState("DEFAULT")
  const [valueHwIdElse, setValueHwIdElse] = useState("")
  const [selectHwIdElse, setSelectHwIdElse] = useState(true)
  const [valueTypeHwIdElse, setValueTypeHwIdElse] = useState("")
  const [statusElse, setStatusElse] = useState()
  const [textElse, setTextElse] = useState("")
  const [freqElse, setFreqElse] = useState(0)

  // req
  const [urlLeft, setUrlLeft] = useState("")
  const [urlThen, setUrlThen] = useState("")
  const [urlElse, setUrlElse] = useState("")

  const handleSend = () => {

    const reqSearch = {
      "id": "FE_CC8Project212",
      "url": URLFE,
      "date": moment().utc(true).toISOString(),
      "create":{
        "if":{
          "left": {
            "url": urlLeft,
            "id": valueHwId
          },
          "condition": valueCondIF,
          "right": {}
        }

      }
    }

    if( valueFreqIF > 1000){
      Object.assign(reqSearch.create.if.left, {"freq": valueFreqIF})
    } else if (urlLeft !== URLBS) {
      Object.assign(reqSearch.create.if.left, {"freq": 1000})
    }

    if( typeRight === "sensor" || typeRight === "freq" || typeRight === "status"){
      const rightObj = JSON.parse( `{ "${typeRight}": ${valueRight}}`)
      Object.assign(reqSearch.create.if.right, rightObj)
    } else if (typeRight === "text"){
      const rightObj = JSON.parse( `{ "${typeRight}": "${valueRight}"}`)
      Object.assign(reqSearch.create.if.right, rightObj)
    }

    if( valueIdPTThen !== "DEFAULT" && valueHwIdThen && (freqThen !== 0 || textThen !== "" || statusThen !== undefined) ){
      var thenObj =
        `{
          "then": {
            "url": "${urlThen}",
            "id": "${valueHwIdThen}", `

      switch (valueTypeHwIdThen) {
        case "input":
          thenObj += `"freq": ${freqThen} }}`
          break

        case "output":
          if(textThen !== "" && statusThen !== undefined){
            thenObj += `"text": "${textThen}", "status": ${statusThen} }}`
          } else if (textThen !== ""){
            thenObj += `"text": "${textThen}" }}`
          } else if (statusThen !== undefined){
            thenObj += `"status": ${statusThen} }}`
          }
          break
        default:
          break
      }

      Object.assign(reqSearch.create, JSON.parse(thenObj))

    }

    if( valueIdPTElse !== "DEFAULT" && valueHwIdElse && (freqElse !== 0 || textElse !== "" || statusElse !== undefined) ){
      var elseObj =
        `{
          "else": {
            "url": "${urlElse}",
            "id": "${valueHwIdElse}", `

      switch (valueTypeHwIdElse) {
        case "input":
          elseObj += `"freq": ${freqElse} }}`
          break

        case "output":
          if(textElse !== "" && statusElse !== undefined){
            elseObj += `"text": "${textElse}", "status": ${statusElse} }}`
          } else if (textElse !== ""){
            elseObj += `"text": "${textElse}" }}`
          } else if (statusElse !== undefined){
            elseObj += `"status": ${statusElse} }}`
          }
          break
        default:
          break
      }

      Object.assign(reqSearch.create, JSON.parse(elseObj))

    }

    fetch('http://' + URLBACKUP + '/create',{
      method: "POST",
      body: JSON.stringify(reqSearch),
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      }
    })
    .then( resFetch => resFetch.json())
    .then(datos => {
        M.toast({html: `${datos.status}`})

        fetch( 'http://' + URLBACKUP + '/create')
        .then( resFetch => resFetch.json())
        .then(datos => {
          handleUpdateEvents(datos)
        })
        .catch( err => {
          console.log("[ERROR][GET] fetch Events: " + err)
          M.toast({html: '[ERROR][GET] fetch Events'})
        })

        setValueIdPT("DEFAULT")
        setValueHwId("")
        setSelectHwId(true)
        setValueFreqIF(0)
        setValueCondIF("")
        setValueTypeHwId("")
        setTypeRight("")
        setValueRight()

        setValueIdPTThen("DEFAULT")
        setValueHwIdThen("")
        setSelectHwIdThen(true)
        setValueTypeHwIdThen("")
        setStatusThen()
        setTextThen("")
        setFreqThen(0)

        setValueIdPTElse("DEFAULT")
        setValueHwIdElse("")
        setSelectHwIdElse(true)
        setValueTypeHwIdElse("")
        setStatusElse()
        setTextElse("")
        setFreqElse(0)

        setUrlLeft("")
        setUrlThen("")
        setUrlElse("")


    })
    .catch( err => {
      console.log("[ERROR][POST] fetch Event to anywhere: " + err)
      M.toast({html: '[ERROR][POST] fetch Event to anywhere'})
    })

  }

  useEffect( () => {
    var elems = document.querySelectorAll('select')
    M.FormSelect.init(elems)

    if(valueHwId !== ""){
      info.map( infoElem => {

        if(infoElem.idPT === valueIdPT){
          setUrlLeft(infoElem.url)
          Object.keys(infoElem.hardware).map(
            key => {
              if(key === valueHwId){
                setValueTypeHwId(infoElem.hardware[key].type)
              }
              return null
            }
          )
        }
        return null
      })
    }

    if(valueHwIdThen !== ""){
      info.map( infoElem => {

        if(infoElem.idPT === valueIdPTThen){
          setUrlThen(infoElem.url)
          Object.keys(infoElem.hardware).map(
            key => {
              if(key === valueHwIdThen){
                setValueTypeHwIdThen(infoElem.hardware[key].type)
              }
              return null
            }
          )
        }
        return null
      })
    }

    if(valueHwIdElse !== ""){
      info.map( infoElem => {

        if(infoElem.idPT === valueIdPTElse){
          setUrlElse(infoElem.url)
          Object.keys(infoElem.hardware).map(
            key => {
              if(key === valueHwIdElse){
                setValueTypeHwIdElse(infoElem.hardware[key].type)
              }
              return null
            }
          )
        }
        return null
      })
    }


  },[info, valueIdPT, valueHwId, valueTypeHwId, typeRight, valueRight, valueIdPTThen, valueHwIdThen, valueTypeHwIdThen, textThen, valueIdPTElse, valueHwIdElse, valueTypeHwIdElse, textElse])

  return (
    <div className="row">

      {/* IF */}
      <div className="col s12">
        <h5>IF</h5>

        <div className="input-field col s5">
          <select className="browser-default" value={valueIdPT} onChange={ (e) => {
            setValueIdPT(e.target.value)
            setSelectHwId(false)
            setValueHwId("")
            setValueFreqIF(0)
          }}>
            <option value="DEFAULT" disabled>Choose </option>
            <OptionIdPT info={info}/>
          </select>
          <label className="active">Base Station</label>
        </div>

        <div className="input-field col s5">
          {
            valueIdPT === "DEFAULT" ?
            (
              <select className="browser-default" defaultValue="DEFAULT" disabled>
                <option value="DEFAULT" disabled></option>
              </select>
            )
            :
            (
              <select className="browser-default" defaultValue="DEFAULT" onChange={ (e) => {
                  setSelectHwId(true)
                  setValueHwId(e.target.value)
                  setTypeRight("")
                  setValueTypeHwId("")
              }}>
                {
                  selectHwId? (<option value="DEFAULT" disabled>Choose</option>) : (<option value="DEFAULT" disabled selected>Choose</option>)
                }
                <OptionHw info={info} idPT={valueIdPT}/>
              </select>
            )
          }
          <label className="active">Hardware ID</label>
        </div>

        <div className="input-field col s2">
          {
            valueIdPT === "PT_CC8Project212" || valueIdPT === "DEFAULT"?
            (<input id="freq_if" type="number"  disabled/>)
            :
            (<input id="freq_if" type="number"  onChange={(e) => {
              setValueFreqIF(e.target.value)
            }}/>)
          }
          <label htmlFor="freq_if">Freq</label>
        </div>

      </div>
      <div className="col s12">
        <div className="input-field col s4">
          {(()=> {
              switch (valueTypeHwId) {
                case "input":
                  return (
                    <select className="browser-default" defaultValue="DEFAULT" onChange={ (e) => {
                      setValueCondIF(e.target.value)}}>
                      <option value="DEFAULT" disabled selected>Choose</option>
                      <option value="="> &#61; </option>
                      <option value="!="> &#33;&#61; </option>
                      <option value="<"> &#60; </option>
                      <option value="<="> &#60;&#61; </option>
                      <option value=">"> &#62; </option>
                      <option value=">="> &#62;&#61; </option>
                    </select>
                  )
                case "output":
                  return (
                    <select className="browser-default" defaultValue="DEFAULT" onChange={ (e) => {
                      setValueCondIF(e.target.value)}}>
                      <option value="DEFAULT" disabled selected>Choose</option>
                      <option value="="> &#61; </option>
                      <option value="!="> &#33;&#61; </option>
                    </select>
                  )
                default:
                  return(
                    <select className="browser-default" defaultValue="DEFAULT">
                      <option value="DEFAULT" disabled>Choose</option>
                    </select>
                  )
              }
          })()}
          <label className="active">Condition</label>
        </div>

        <div className="input-field col s4">
          {(()=> {
            switch (valueTypeHwId) {
              case "input":
                return (
                  <select className="browser-default" defaultValue="DEFAULT" onChange={ (e) => {
                    setTypeRight(e.target.value)}}>
                    <option value="DEFAULT" disabled selected>Choose</option>
                    <option value="sensor"> sensor </option>
                    <option value="freq"> freq </option>
                  </select>
                )
              case "output":
                return (
                  <select className="browser-default" defaultValue="DEFAULT" onChange={ (e) => {
                    setTypeRight(e.target.value)}}>
                    <option value="DEFAULT" disabled selected>Choose</option>
                    <option value="status"> status </option>
                    <option value="text"> text </option>
                  </select>
                )
              default:
                return(
                  <select className="browser-default" defaultValue="DEFAULT">
                    <option value="DEFAULT" disabled>Choose</option>
                  </select>
                )
            }
          })()}
        </div>

        {(()=> {
          switch (typeRight) {
            case "sensor":
            case "freq":
              return (
                <div className="input-field col s2">
                  <input id="freq_if_right" type="number"  onChange={(e) => {
                    setValueRight(e.target.valueAsNumber)
                  }}/>
                </div>
                )
            case "text":
              return (
                <div className="input-field col s2">
                  <input id="text_if_right" type="text"  onChange={(e) => {
                    setValueRight(e.target.value)
                  }}/>
                  <label htmlFor="text_if_right">Text</label>
                </div>
              )
            case "status":
              return (
                <div className="input-field col s2">
                  <input id="status_if_right" type="text"  placeholder="true or false" onChange={(e) => {
                    setValueRight(e.target.value === "true"? true : false)
                  }}/>
                  <label className="active" htmlFor="status_if_right">Status</label>
                </div>
              )
            default:
              return null
          }
        })()}

      </div>

      <div className="divider col s12"></div>

      {/* Then */}
      <div className="col s12">
        <h5>Then</h5>

        <div className="input-field col s6 m4">
          {
            valueRight === undefined ?
            (
              <select className="browser-default" value={valueIdPTThen} disabled>
                <option value="DEFAULT" disabled></option>
              </select>
            )
            :
            (
              <select className="browser-default" value={valueIdPTThen} onChange={ (e) => {
                setValueIdPTThen(e.target.value)
                setSelectHwIdThen(false)
                setValueHwIdThen("")
              }}>
                <option value="DEFAULT" disabled>Choose </option>
                <OptionIdPT info={info}/>
              </select>
            )
          }
          <label className="active">Base Station</label>
        </div>

        <div className="input-field col s6 m4">
          {
            valueIdPTThen === "DEFAULT" ?
            (
              <select className="browser-default" defaultValue="DEFAULT" disabled>
                <option value="DEFAULT" disabled></option>
              </select>
            )
            :
            (
              <select className="browser-default" defaultValue="DEFAULT" onChange={ (e) => {
                  setSelectHwIdThen(true)
                  setValueHwIdThen(e.target.value)
                  setStatusThen(undefined)
                  setTextThen("")
                  setFreqThen(0)
              }}>
                {
                  selectHwIdThen? (<option value="DEFAULT" disabled>Choose</option>) : (<option value="DEFAULT" disabled selected>Choose</option>)
                }
                <OptionHw info={info} idPT={valueIdPTThen}/>
              </select>
            )
          }
          <label className="active">Hardware ID</label>
        </div>

        {(() => {
          switch (valueTypeHwIdThen) {
            case "input":
              return (
                <div className="col s6 m4">
                  <div className="input-field col s12">
                    <input id="freq_then" type="number" onChange={(e) => {
                    setFreqThen(e.target.valueAsNumber)
                    }}/>
                    <label className="active" htmlFor="freq_then">Freq</label>
                  </div>
                </div>
              )

            case "output":
              return (
                <div className="col s12 m4">

                  <div className="input-field col s6">
                    <input id="text_then" type="text" value={textThen} onChange={(e) => {
                    setTextThen(e.target.value)
                    }}/>
                    <label htmlFor="text_then">text</label>
                  </div>

                  <div className="input-field col s6">
                    <input id="status_then" type="text" placeholder="true or false" onChange={(e) => {
                    setStatusThen(e.target.value === "true"? true : false)
                    }}/>
                    <label className="active" htmlFor="status_then">Status</label>
                  </div>

                </div>
              )
            default:
              return null
          }
        })()}

      </div>

      <div className="divider col s12"></div>

      {/* else */}
      <div className="col s12">
        <h5>Else</h5>

        <div className="input-field col s6 m4">
          {
            valueRight === undefined ?
            (
              <select className="browser-default" value={valueIdPTElse} disabled>
                <option value="DEFAULT" disabled></option>
              </select>
            )
            :
            (
              <select className="browser-default" value={valueIdPTElse} onChange={ (e) => {
                setValueIdPTElse(e.target.value)
                setSelectHwIdElse(false)
                setValueHwIdElse("")
              }}>
                <option value="DEFAULT" disabled>Choose </option>
                <OptionIdPT info={info}/>
              </select>
            )
          }
          <label className="active">Base Station</label>
        </div>

        <div className="input-field col s6 m4">
          {
            valueIdPTElse === "DEFAULT" ?
            (
              <select className="browser-default" defaultValue="DEFAULT" disabled>
                <option value="DEFAULT" disabled></option>
              </select>
            )
            :
            (
              <select className="browser-default" defaultValue="DEFAULT" onChange={ (e) => {
                  setSelectHwIdElse(true)
                  setValueHwIdElse(e.target.value)
                  setStatusElse(undefined)
                  setTextElse("")
                  setFreqElse(0)
              }}>
                {
                  selectHwIdElse? (<option value="DEFAULT" disabled>Choose</option>) : (<option value="DEFAULT" disabled selected>Choose</option>)
                }
                <OptionHw info={info} idPT={valueIdPTElse}/>
              </select>
            )
          }
          <label className="active">Hardware ID</label>
        </div>

        {(() => {
          switch (valueTypeHwIdElse) {
            case "input":
              return (
                <div className="col s6 m4">
                  <div className="input-field col s12">
                    <input id="freq_Else" type="number"  onChange={(e) => {
                    setFreqElse(e.target.valueAsNumber)
                    }}/>
                    <label className="active" htmlFor="freq_Else">Freq</label>
                  </div>
                </div>
              )

            case "output":
              return (
                <div className="col s12 m4">

                  <div className="input-field col s6">
                    <input id="text_Else" type="text"  value={textElse} onChange={(e) => {
                    setTextElse(e.target.value)
                    }}/>
                    <label htmlFor="text_Else">text</label>
                  </div>

                  <div className="input-field col s6">
                    <input id="status_then" type="text"  placeholder="true or false" onChange={(e) => {
                    setStatusElse(e.target.value === "true"? true : false)
                    }}/>
                    <label className="active" htmlFor="status_then">Status</label>
                  </div>

                </div>
              )

            default:
              return null
          }
        })()}


      </div>


      {/* Floating Button */}
      <div className="fixed-action-btn">
        <a className={"btn-floating btn-large waves-effect waves-light purple darken-4 " +
        ((freqElse !== 0 || textElse !== "" || statusElse !== undefined ||
        freqThen !== 0 || textThen !== "" || statusThen !== undefined)? "": "disabled")}
          href="#!"
          onClick={() => handleSend()}
        >
          <i className="material-icons">backup</i>
        </a>
      </div>

    </div>
  )
}

const mapStateToProps = state =>({
  info: state.info
})

const mapDispatchToProps = dispatch =>({
  handleUpdateEvents(datos){
    dispatch({
      type: "UPDATEEVENTSARRAY",
      datos
    })
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(EventTab)
