import React, {useState, useEffect} from 'react'
import { connect } from "react-redux"
import {Chart} from 'react-google-charts'

const RealtimeGraphics = ({graphicRT01}) => {

  const [dataGraphic1, setDataGraphic1] = useState([])
  const [dataGraphic2, setDataGraphic2] = useState([])
  const [options1, setOptions1] = useState({})
  const [options2, setOptions2] = useState({})
  const [tipoGraphic, setTipoGraphic] = useState("SteppedAreaChart")

  useEffect( () => {

    if(graphicRT01.length !== 0){
      if(graphicRT01.search.type === "input"){
        const columns = [
          {type:'date', label:'Instante'},
          {type:'number', label:'sensor'}
        ]

        const columns2 = [
          {type:'date', label:'Instante'},
          {type:'number', label:'Freq'}
        ]

        let rows = []
        let rows2 = []
        for( var row in graphicRT01.data){

          rows.push( [new Date(row) , graphicRT01.data[row].sensor] )
          rows2.push( [new Date(row) , graphicRT01.data[row].freq] )
        }

        const options = {
          hAxis: {
            title: 'Time',
          },
          vAxis:{
            title: 'Sensor',
            minValue: 0
          },
          colors: ['#4a148c'],
          legend: 'none'
        }

        const options2 = {
          hAxis: {
            title: 'Time',
          },
          vAxis:{
            title: 'Frequence',
            minValue: 0
          },
          colors: ['#4a148c'],
          legend: 'none'
        }

        setOptions1(options)
        setDataGraphic1([columns, ...rows])

        setTipoGraphic("AreaChart")
        setOptions2(options2)
        setDataGraphic2([columns2, ...rows2])

      } else {

        const columns = [
          {type:'date', label:'Instante'},
          {type:'number', label:'status'}
        ]

        const columns2 = [
          {type:'string', label:'Text'},
          {type:'number', label:'Cantidad'}
        ]

        let rows = []
        let rows2 = []
        let textRows = []
        for( var row2 in graphicRT01.data){

          rows.push([
            new Date(row2),
            (graphicRT01.data[row2].status === true)? 1:0
          ])

          rows2.push(graphicRT01.data[row2].text)
        }

        const mySet = new Set(rows2)
        for(var item of mySet) {
          var count = 0

          for(var item2 in graphicRT01.data){
            if( graphicRT01.data[item2].text === item){
              count++
            }
          }

          textRows.push([item, count])
        }

        const options = {
          hAxis: {
            title: 'Time'
          },
          vAxis:{
            title: 'Status',
            minValue: 0
          },
          legend: 'none'
        }

        const options2 = {
          hAxis: {
            title: 'Count'
          },
          vAxis:{
            title: 'Text'
          },
          legend: 'none'
        }


        setOptions1(options)
        setDataGraphic1([columns, ...rows])

        setTipoGraphic("BarChart")
        setOptions2(options2)
        setDataGraphic2([columns2, ...textRows])

      }
    }

  }, [graphicRT01])

  return (
    <div className="col s12">
      {
        graphicRT01.length !== 0 ?
        (
          <div className="row">
            <h6>Sensor de Fuerza Peso</h6>
            <div className="col s12 m6">
              <Chart
                chartType="AreaChart"
                data={dataGraphic1}
                width="100%"
                height="100%"
                options={options1}
                loader={
                  <div className="progress">
                    <div className="indeterminate"></div>
                  </div>
                }
              />
            </div>

            <div className="col s12 m6">
              <Chart
                chartType={tipoGraphic}
                data={dataGraphic2}
                width="100%"
                height="100%"
                options={options2}
                loader={
                  <div className="progress">
                    <div className="indeterminate"></div>
                  </div>
                }
              />
            </div>

          </div>
        )
        :
        (
          null
        )
      }
    </div>
  )
}

const mapStateToProps = state =>({
  graphicRT01: state.graphicRT01
})

const mapDispatchToProps = dispatch =>({})

export default connect(mapStateToProps, mapDispatchToProps)(RealtimeGraphics)
