import React, {useState, useEffect} from 'react'
import { connect } from "react-redux";
import * as moment from 'moment'
import M from 'materialize-css'
import './index.css'
import {URLFE, URLBS, URLBACKUP} from '../../shared/js/globalV'
import SearchGraphic from '../SearchGraphic';

const OptionIdPT = ({info}) => {

  const option = info.map( infoElem => (
    <option key={infoElem.idPT} value={infoElem.idPT}> {infoElem.idPT} </option>
  ))

  return option
}

const OptionHw = ({info, idPT}) => {

  const option = info.map( infoElem => {

    var optionsHardware;

    if(infoElem.idPT === idPT) {

      optionsHardware = Object.keys(infoElem.hardware).map( key => {
        return (
          <option key={key} value={key}> {infoElem.hardware[key].tag} </option>
        )
      })
    }

    return optionsHardware
  })

  return option
}

const SearchHwTab = ({info, handleSaveSearch}) => {

  const [valueIdPT, setValueIdPT] = useState("")
  const [valueHwId, setValueHwId] = useState("")
  const [startDate, setStartDate] = useState("")
  const [startTime, setStartTime] = useState({})
  const [finishDate, setFinishDate] = useState("")
  const [finishTime, setFinishTime] = useState({})
  const [selectHwId, setSelectHwId] = useState(true)


  const handleSearch = () => {
    const reqSearch = {
      "id": "FE_CC8Project212",
      "url": URLFE,
      "date": moment().utc(true).toISOString(),
      "search": {
        "id_hardware": valueHwId,
        "start_date":  `${startDate}T${startTime.hour < 10? '0':''}${startTime.hour}:${startTime.minute < 10? '0':''}${startTime.minute}:00.000Z`,
        "finish_date": `${finishDate}T${finishTime.hour < 10? '0':''}${finishTime.hour}:${finishTime.minute < 10? '0':''}${finishTime.minute}:00.000Z`
      }
    }

    var urlToSearch = info.find( infoElem => ( infoElem.idPT === valueIdPT))

    if (urlToSearch.url === URLBS) {

      fetch('http://' + URLBACKUP + '/search',{
        method: "POST",
        body: JSON.stringify(reqSearch),
        headers: {
          "Content-Type": "application/json; charset=utf-8"
        }
      })
      .then( resFetch => resFetch.json())
      .then(datos => {
        handleSaveSearch(datos)
      })
      .catch( err => {
        console.log("[ERROR][POST] fetch Search to anywhere: " + err)
        M.toast({html: '[ERROR][POST] fetch Search to anywhere'})
      })
    } else {
      fetch('http://' + urlToSearch.url + '/search',{
        method: "POST",
        body: JSON.stringify(reqSearch),
        headers: {
          "Content-Type": "application/json; charset=utf-8"
        }
      })
      .then( resFetch => resFetch.json())
      .then(datos => {
        handleSaveSearch(datos)
      })
      .catch( err => {
        console.log("[ERROR][POST] fetch Search to anywhere: " + err)
        M.toast({html: '[ERROR][POST] fetch Search to anywhere'})
      })

    }
  }

  useEffect( () => {

    var elems = document.querySelectorAll('select')
    var elems2 = document.querySelectorAll('.datestartset')
    var elems3 = document.querySelectorAll('.timestartset')
    var elems4 = document.querySelectorAll('.datefinishset')
    var elems5 = document.querySelectorAll('.timefinishset')

    M.FormSelect.init(elems)

    M.Datepicker.init(elems2, {
      onSelect: (date) => setStartDate(`${date.getFullYear()}-${(date.getMonth()+1) < 10? '0':''}${date.getMonth()+1}-${date.getDate() < 10? '0':''}${date.getDate()}`),
      format: 'dd-mm-yyyy'
    })

    M.Timepicker.init(elems3, {
      onSelect: (hour, minute) => setStartTime({hour, minute}),
      twelveHour: false
    })

    M.Datepicker.init(elems4, {
      onSelect: (date) => setFinishDate(`${date.getFullYear()}-${(date.getMonth()+1) < 10? '0':''}${date.getMonth()+1}-${date.getDate() < 10? '0':''}${date.getDate()}`),
      format: 'dd-mm-yyyy'
    })

    M.Timepicker.init(elems5, {
      onSelect: (hour, minute) => setFinishTime({hour, minute}),
      twelveHour: false
    })

  },[info, valueIdPT])

  return (
    <div className="row">
      <div className="input-field col s6 m3">
        <select className="browser-default" defaultValue="DEFAULT" onChange={ (e) => {
          setValueIdPT(e.target.value)
          setSelectHwId(false)
          }}>
          <option value="DEFAULT" disabled>Choose</option>
          <OptionIdPT info={info}/>
        </select>
        <label className="active">Base Station</label>
      </div>

      <div className="input-field col s6 m3">
        {
          !valueIdPT ?
          (
            <select className="browser-default" defaultValue="DEFAULT" disabled>
              <option value="DEFAULT" disabled></option>
            </select>
          )
          :
          (
            <select className="browser-default" defaultValue="DEFAULT" onChange={ (e) => {
                setSelectHwId(true)
                setValueHwId(e.target.value)
            }}>
              {
                selectHwId? (<option value="DEFAULT" disabled>Choose</option>) : (<option value="DEFAULT" disabled selected>Choose</option>)
              }
              <OptionHw info={info} idPT={valueIdPT}/>
            </select>
          )
        }
        <label className="active">Hardware ID</label>
      </div>

      <div className="col s12">
          <div className="col s6">
            <h6>Start</h6>
            <div className="col s6">
              <input type="text" placeholder="Fecha" className="datepicker datestartset"/>
            </div>

            <div className="col s6">
              <input type="text" placeholder="Hora" className="timepicker timestartset"/>
            </div>
          </div>

          <div className="col s6">
            <h6>Finish</h6>
            <div className="col s6">
              <input type="text" placeholder="Fecha" className="datepicker datefinishset"/>
            </div>

            <div className="col s6">
              <input type="text" placeholder="Hora" className="timepicker timefinishset"/>
            </div>
          </div>
        </div>

      <div className="col s12">
        <a className={"btn-floating btn-large waves-effect waves-light purple darken-4 right " + (Object.keys(finishTime).length === 0? "disabled":"")} href="#!" onClick={ handleSearch }><i className="material-icons">search</i></a>
      </div>

      <SearchGraphic/>
    </div>
  )
}

const mapStateToProps = state =>({
  info: state.info
})

const mapDispatchToProps = dispatch =>({

  handleSaveSearch(datos){
    dispatch({
      type: 'SAVESEARCH',
      datos
    })
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(SearchHwTab)
