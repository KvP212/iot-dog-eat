import React, {Component} from 'react';
import M from 'materialize-css'
import { BrowserRouter as Router} from "react-router-dom";
import { Provider } from "react-redux";
import store from './store'

import Content from './shared/components/layout/Content';
import Header from './shared/components/layout/Header';

class App extends Component {

  componentDidMount(){ M.AutoInit() }

  render(){
    return (
      <Router>
        <Provider store={store}>

          <header>
            <Header/>
          </header>

          <main>
            <Content/>
          </main>

        </Provider>
      </Router>
    );
  }
}

export default App;
