//WIFI
const URLBACKUP = '10.106.1.135:4040'
const URLBS = '10.106.1.135:3030'
const URLFE = '10.106.1.135:3000'

//LOCAL
// const URLBACKUP = 'localhost:4040'
// const URLBS = 'localhost:3030'
// const URLFE = 'localhost:3000'

export {URLBACKUP, URLBS, URLFE}
