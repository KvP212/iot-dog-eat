import React from 'react'

const Header = () => {
  return (
    <div>
      <nav className="nav-extended">
        <div className="nav-wrapper">
          <a href="!#" className="brand-logo"><i className="large material-icons">wifi_tethering</i> IoT </a>
        </div>
        <div className="nav-content">
          <ul className="tabs tabs-transparent">
            <li className="tab"><a className="active" href="!#realtimeHw">Realtime</a></li>
            <li className="tab"><a href="!#test1">Info</a></li>
            <li className="tab"><a href="!#test2">Search</a></li>
            <li className="tab"><a href="!#test3">Events</a></li>
            <li className="tab"><a href="!#test4">EXEC</a></li>
          </ul>
        </div>
      </nav>
    </div>
  )
}

export default Header
