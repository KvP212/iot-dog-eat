import React, { Component } from 'react'
import {connect} from 'react-redux'
import InfoHwsTab from '../../../../components/InfoHwsTab'
import {URLBACKUP, URLFE} from '../../../js/globalV'
import SearchHwTab from '../../../../components/SearchHwTab'
import EventTab from '../../../../components/EventTab'
import ExecTab from '../../../../components/ExecTab'
import moment from 'moment'
import RealtimeGraphics from '../../../../components/RealtimeGraphics'
import RealtimeGraphics02 from '../../../../components/RealtimeGraphics02'
import RealtimeGraphics03 from '../../../../components/RealtimeGraphics03'
import ChangeDirecto from '../../../../components/ChangeDirecto'
import M from 'materialize-css'

function getInfos(handleDispatch){
  fetch( 'http://' + URLBACKUP + '/info')
  .then( resFetch => resFetch.json())
  .then(datos => {
    handleDispatch({
      type: 'UPDATEINFOSARRAY',
      datos: datos
    })
  })
  .catch( err => console.log("[ERROR][GET] fetch Infos: " + err))
}

function getEvents(handleDispatch){
  fetch( 'http://' + URLBACKUP + '/create')
  .then( resFetch => resFetch.json())
  .then(datos => {
    handleDispatch({
      type: 'UPDATEEVENTSARRAY',
      datos: datos
    })
  })
  .catch( err => console.log("[ERROR][GET] fetch Events: " + err))
}

function getRealTime(handleDispatch) {
  const startDate = moment().hour( moment().hour() - 1).utc(true).toISOString()
  const finishDate = moment().utc(true).toISOString()

  const reqSearch01 = {
    "id": "FE_CC8Project212",
    "url": URLFE,
    "date": moment().utc(true).toISOString(),
    "search": {
      "id_hardware": "id01",
      "start_date":  startDate,
      "finish_date": finishDate
    }
  }

  const reqSearch02 = {
    "id": "FE_CC8Project212",
    "url": URLFE,
    "date": moment().utc(true).toISOString(),
    "search": {
      "id_hardware": "id02",
      "start_date":  startDate,
      "finish_date": finishDate
    }
  }

  const reqSearch03 = {
    "id": "FE_CC8Project212",
    "url": URLFE,
    "date": moment().utc(true).toISOString(),
    "search": {
      "id_hardware": "id03",
      "start_date":  startDate,
      "finish_date": finishDate
    }
  }

  fetch('http://' + URLBACKUP + '/search',{
    method: "POST",
    body: JSON.stringify(reqSearch01),
    headers: {
      "Content-Type": "application/json; charset=utf-8"
    }
  })
  .then( resFetch => resFetch.json())
  .then( datos => {
    handleDispatch({
      type: 'SAVESEARCHRT01',
      datos
    })

    fetch('http://' + URLBACKUP + '/search',{
      method: "POST",
      body: JSON.stringify(reqSearch02),
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      }
    })
    .then( resFetch => resFetch.json())
    .then( datos => {
      handleDispatch({
        type: 'SAVESEARCHRT02',
        datos
      })

      fetch('http://' + URLBACKUP + '/search',{
        method: "POST",
        body: JSON.stringify(reqSearch03),
        headers: {
          "Content-Type": "application/json; charset=utf-8"
        }
      })
      .then( resFetch => resFetch.json())
      .then( datos => {
        handleDispatch({
          type: 'SAVESEARCHRT03',
          datos
        })
      })
      .catch( err => console.log("[ERROR][POST] fetch Search to anywhere: " + err))

    })
    .catch( err => console.log("[ERROR][POST] fetch Search to anywhere: " + err))

  })
  .catch( err => console.log("[ERROR][POST] fetch Search to anywhere: " + err))
}

export class Content extends Component {

  constructor(props){
    super(props)
    this.state = {
      infoUrl: ""
    }
  }

  componentDidMount(){
    getInfos(this.props.dispatch)
    getRealTime(this.props.dispatch)
    getEvents(this.props.dispatch)
  }

  infoChangeHandler = (event) => {
    // console.log("infoChangeHandler: " + event.target.value)
    this.setState({infoUrl: event.target.value})
  }

  infoSubmitHandler = () => {

    var data = {
      id: "FE_CC8Project212",
      url: URLFE,
      date: moment().utc(true).toISOString()
    }

    fetch(  'http://' + this.state.infoUrl + '/info', {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      }
    })
    .then( resFetch => resFetch.json())
    .then(datos => {

      fetch( 'http://' + URLBACKUP + '/info',{
        method: "POST",
        body: JSON.stringify(datos),
        headers: {
          "Content-Type": "application/json; charset=utf-8"
        }
      })
      .then( resFetch => resFetch.json())
      .then(datos => {

        if( datos.status === "Hardware Saved"){
          M.toast({html: 'Hardware Saved'})
          getInfos(this.props.dispatch)
          this.setState({infoUrl: ""})
        }

      })
      .catch( err => {
        console.log("[ERROR][POST] fetch Infos to Backup: " + err)
        M.toast({html: '[ERROR][POST] fetch Infos to Backup'})
      })

    })
    .catch( err => {
      console.log("[ERROR][POST] fetch Infos: " + err)
      M.toast({html: '[ERROR][POST] fetch Infos'})
    })
  }

  render() {
    return (
      <div>
        <div id="realtimeHw" className="col s12">
          <div className="container">
            <h3>Hardware in Realtime</h3>
            <RealtimeGraphics/>
            <div className="col s12 divider"></div>
            <RealtimeGraphics02/>
            <div className="col s12 divider"></div>
            <RealtimeGraphics03/>
            <div className="col s12 divider"></div>
            <ChangeDirecto/>

            {/* Floating Button */}
            <div className="fixed-action-btn">
              <a className={"btn-floating btn-large waves-effect waves-light purple darken-4"}
                href="#!"
                onClick={() => getRealTime(this.props.dispatch)}
              >
                <i className="material-icons">loop</i>
              </a>
            </div>
          </div>
        </div>

        <div id="test1" className="col s12">
          <div className="container">

            <h2>Hardwares Info</h2>

            <div className="row">
              <div className="col s12">

                <div className="input-field inline">
                  <i className="material-icons prefix">swap_vertical_circle</i>
                  <input id="info_hardwares" type="text" onChange={this.infoChangeHandler}/>
                  <label htmlFor="info_hardwares">URL Plataforma</label>
                </div>

                <a className="waves-effect waves-light btn-small purple darken-4" href="#!" onClick={this.infoSubmitHandler}>info</a>
              </div>
            </div>

            <InfoHwsTab/>
          </div>
        </div>

        <div id="test2" className="col s12">
          <div className="container">

            <h2>Hardwares Search</h2>
            <SearchHwTab/>
            {/* Graficas */}
          </div>
        </div>

        <div id="test3" className="col s12">
          <div className="container">

            <h2>Events</h2>

            <EventTab/>

          </div>
        </div>

        <div id="test4" className="col s12">
          <ExecTab/>
        </div>
      </div>
    )
  }
}

export default connect()(Content)
