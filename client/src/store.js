import { createStore, applyMiddleware } from "redux";

const initialState = {
  info: [],
  graphic: [],
  events:[],
  idEvent: "",
  eventObj:{},
  isUpdateCreate: false,
  graphicRT01:[],
  graphicRT02:[],
  graphicRT03:[]
}

const reducerQuantum = (state = initialState, action) => {

  switch (action.type) {
    case "UPDATEINFOSARRAY":
      return{
        ...state,
        info: action.datos
      }
    case "SAVESEARCH":
      return{
        ...state,
        graphic: action.datos
      }
    case "UPDATEEVENTSARRAY":
      return{
        ...state,
        events: action.datos
      }

    case "SAVE_IDEVENT":
      return{
        ...state,
        idEvent: action.idEvent,
        eventObj: state.events.find( eventElem => (eventElem.idEvent === action.idEvent) )
      }

    case "ISUPDATE":
      return{
        ...state,
        isUpdateCreate: (state.isUpdateCreate === true)? false:true
      }

    case "SAVESEARCHRT01":
      return{
        ...state,
        graphicRT01: action.datos
      }

    case "SAVESEARCHRT02":
      return{
        ...state,
        graphicRT02: action.datos
      }

    case "SAVESEARCHRT03":
      return{
        ...state,
        graphicRT03: action.datos
      }

    default:
      return state
  }

}

function logger({ getState }) {
  return next => action => {
    console.log('will dispatch', action)

    // Call the next dispatch method in the middleware chain.
    const returnValue = next(action)

    console.log('state after dispatch', getState())

    // This will likely be the action itself, unless
    // a middleware further in chain changed it.
    return returnValue
  }
}

export default createStore(reducerQuantum, applyMiddleware(logger));
