# IoT Dog Eat

_Controlando la comida que se le da al perro, conforme los niveles que se recomindan por las bolsas de concentrado_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Node.js
Express - npm install express --save
Morgan - npm install morgan --save
Mongodb - npm install mongoose --save
React
```

## Ejecutando las pruebas ⚙️

_Ejecutar las pruebas automatizadas para este sistema_

_Dev_
```
npm run dev
npm test
```
_Production_
```
1) npm bb
2) npm serve
```

## Deployment 📦

```
Nodemon - npm install nodemon -D
Babel - npm install --save-dev @babel/cli @babel/core @babel/preset-env
Mocha - npm install --save-dev mocha
Babel/Register - npm install --save-dev @babel/register
```

## Construido con 🛠️

_Frameworks_

* [Nodejs](https://nodejs.org/es/) - El Framework Back-End
* [Express](https://expressjs.com/) - El Framework Back-End
* [mongo DB](https://github.com/KvP212) - Manejador de DB
* [React](https://es.reactjs.org/) - El Framework Front-End

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Kev Pineda** - *Trabajo Inicial y Desarrollo* - [KvP212](https://gitlab.com/KvP212)

También puedes mirar la lista de todos los [contribuyentes](https://gitlab.com/KvP212/iot-dog-eat/-/project_members) quíenes han participado en este proyecto.
