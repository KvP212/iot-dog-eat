#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <HX711.h>
#include <ArduinoHttpClient.h>
#include <ArduinoJson.h>
#include <../lib/arduino_secrets.h>

#define TASKER_MAX_TASKS 6
#include "Tasker.h"
Tasker tasker;

char ssid[] = SECRET_SSID; //  your network SSID (name)
char pass[] = SECRET_PASS;    // your network password (use for WPA, or use as key for WEP)
// int keyIndex = 0;            // your network key Index number (needed only for WEP)

//SERVER
int status = WL_IDLE_STATUS;
// if you don't want to use DNS (and reduce your sketch size)
// use the numeric IP instead of the name for the server:
//IPAddress server(74,125,232,128);  // numeric IP for Google (no DNS)
char server[] = "10.106.1.135";
int port = 3030;
// Initialize the Ethernet client library
// with the IP address and port of the server
// that you want to connect to (port 80 is default for HTTP):
WiFiClient wclient;
HttpClient client = HttpClient(wclient, server, port);
const String contentType = "application/json";
const int capacity = JSON_OBJECT_SIZE(9);
StaticJsonDocument<capacity> doc;
JsonObject obj = doc.to<JsonObject>(); // Create a JsonObject
const int capacityRes = JSON_OBJECT_SIZE(9);
StaticJsonDocument<capacityRes> docRes;

//Hardware CONFIG
const int LOADCELL_DOUT_PIN = D1;
const int LOADCELL_SCK_PIN = D2;
HX711 scale;
const int MOTOR_PIN = D8;
const int RED_PIN = D5;
const int BLUE_PIN = D6;
const int GREEN_PIN = D7;
const int LED_MODULE = D0;

//Global Variables
float peso = 0;
int pesoEntero = 0;
unsigned long freqPL = 100; //Peso Led
unsigned long freqM = 50; //Motor
unsigned long freqGet = 800;
unsigned long freqWifi = 30000;
bool statusLED = false;
String textLED = "";
bool statusMotor = false;
String textMotor = "";

//CallBack
void taskPostState();
void taskGetChanges();
void taskPeso();
void taskMotor();
void taskLED();

void color(int rojo, int verde, int azul){
  analogWrite(RED_PIN, map(rojo, 0, 255, 0, 1023));
  analogWrite(BLUE_PIN, map(azul, 0, 255, 0, 1023));
  analogWrite(GREEN_PIN, map(verde, 0, 255, 0, 1023));
}

void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void taskPostState(){

  String postData = "";

  obj["sensor"] = pesoEntero;
  obj["freq"] = freqWifi;
  obj["statusMotor"] = statusMotor;
  obj["textMotor"] = textMotor.c_str();
  obj["statusLED"] = statusLED;
  obj["textLED"] = textLED.c_str();
  serializeJson(obj, postData);

  client.post("/api/v1/hw_pesos", contentType, postData);
  client.responseStatusCode();
  String response = client.responseBody();

}

void taskGetChanges(){

  digitalWrite(LED_MODULE , HIGH);
  client.get("/api/v1/hw_pesos");
  client.responseStatusCode();
  String response = client.responseBody();

  deserializeJson(docRes, response);
  // DeserializationError err = deserializeJson(docRes, response);
  // // Parse succeeded?
  // if (err) {
  //   Serial.print(F("deserializeJson() returned "));
  //   Serial.println(err.c_str());
  //   return;
  // }

  unsigned long resfreqWifi = docRes["freq"];
  bool resstatusMotor = docRes["statusMotor"];
  bool resstatusLED = docRes["statusLED"];
  const char* restextLED = docRes["textLED"];
  const char* restextMotor = docRes["textMotor"];

  if(resfreqWifi != freqWifi){
    freqWifi = resfreqWifi;
    tasker.setInterval(taskPostState, freqWifi);
  }

  if(resstatusMotor != statusMotor ){
    statusMotor = resstatusMotor;
  }

  if(resstatusLED != statusLED ){
    statusLED = resstatusLED;
  }

  if( !textLED.equals(restextLED)){
    textLED = restextLED;
  }

  if( !textMotor.equals(restextMotor)){
    textMotor = restextMotor;
  }

  digitalWrite(LED_MODULE , LOW);
}

void taskPeso(){

  // Serial.print("Valor de lectura:  ");
  // Serial.println(scale.get_value(10),0);
  // delay(100);

  peso = scale.get_units();
  if(peso < 1){
    pesoEntero = 0;
  } else if (peso > 1000){
    pesoEntero = 1023;
  } else {
    pesoEntero = (int) ((peso * 1023)/1000);
  }

  // Serial.print("Peso: ");
  // Serial.print( peso, 0 );
  // Serial.print(" grams ");
  // Serial.print("PesoEntero: ");
  // Serial.print( pesoEntero);
  // Serial.println();

}

void taskMotor(){
  if(statusMotor == true){

    if(textMotor == "low"){
      analogWrite(MOTOR_PIN, map(85, 0, 255, 0, 1023));
    }

    if(textMotor == "medium"){
      analogWrite(MOTOR_PIN, map(170, 0, 255, 0, 1023));
    }

    if(textMotor == "high"){
      analogWrite(MOTOR_PIN, map(255, 0, 255, 0, 1023));
    }

  } else {
    analogWrite(MOTOR_PIN, 0);
  }

}

void taskLED(){
  if (statusLED == true){

    //config despues, para LED RGB
    if(textLED == "red"){
      color(255, 0, 0);
    }

    if(textLED == "blue"){
      color(0, 0, 255);
    }

    if(textLED == "green"){
      color(0, 255, 0);
    }

    if(textLED == "rg"){
      color(255, 255, 0);
    }

    if(textLED == "rb"){
      color(255, 0, 255);
    }

    if(textLED == "gb"){
      color(0, 255, 255);
    }

  } else {
    color(0, 0, 0);
  }
}

void setup() {
  Serial.begin(115200);

  pinMode(MOTOR_PIN, OUTPUT);
  pinMode(LED_MODULE, OUTPUT);
  pinMode(RED_PIN, OUTPUT);
  pinMode(BLUE_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);

  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);

  // attempt to connect to Wifi network:
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    status = WiFi.begin(ssid, pass);

    // wait 10 seconds for connection:
    delay(10000);
  }
  Serial.println("Connected to wifi");
  printWifiStatus();

  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  // Serial.print("Lectura del valor del ADC:  ");
  // Serial.println(scale.read());
  Serial.println("No ponga ningun  objeto sobre la balanza");
  Serial.println("Destarando...");

  // scale.set_scale(); //La escala por defecto es 1
  // scale.tare(20);	//El peso actual es considerado Tara.
  // Serial.println("Coloque un peso conocido:");
  // Serial.println("...");
  // delay(4000);
  scale.set_scale(837.6212774); // Establecemos la escala
  scale.tare(20);	//El peso actual es considerado Tara.
  Serial.println("Listo para pesar");

  Serial.println("Setup done");
  tasker.setInterval(taskPeso, freqPL);
  tasker.setInterval(taskMotor, freqM);
  tasker.setInterval(taskLED, freqPL);
  tasker.setInterval(taskGetChanges, freqGet);
  tasker.setInterval(taskPostState, freqWifi);

}

void loop() {

  tasker.loop();

}
