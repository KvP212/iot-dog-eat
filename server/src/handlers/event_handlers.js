import fetch from 'node-fetch'
import Createdata from '../models/create_event_data'
import {URLLOCAL} from '../globalV'
import Hwpesodata from '../models/hw_peso_data'
import moment from 'moment'

export default async () => {

  console.log("---EXEC EVENTS---")
  const allEvents = await Createdata.find({"create.if.left.url":URLLOCAL})
  const hwdatasactual = await Hwpesodata.find().sort({$natural:-1}).limit(1)

  var structureChange = {
    "id": "PT_CC8Project212",
    "url": URLLOCAL,
    "date": moment().utc(true).toISOString(),
    "change": {}
  }

  allEvents.forEach( async (element) => {

    var structureid = ""
    var cumpleCond = false

    if( element.create.if.right.freq !== undefined){

      if( element.create.if.condition === '='){

        if(hwdatasactual[0].id01.freq === element.create.if.right.freq){
          cumpleCond = true
        }

      } else if( element.create.if.condition === '!='){

        if(hwdatasactual[0].id01.freq !== element.create.if.right.freq){
          cumpleCond = true
        }

      } else if( element.create.if.condition === '>'){

        if(hwdatasactual[0].id01.freq > element.create.if.right.freq){
          cumpleCond = true
        }

      } else if( element.create.if.condition === '>='){

        if(hwdatasactual[0].id01.freq >= element.create.if.right.freq){
          cumpleCond = true
        }

      } else if( element.create.if.condition === '<'){

        if(hwdatasactual[0].id01.freq < element.create.if.right.freq){
          cumpleCond = true
        }

      } else if( element.create.if.condition === '<='){

        if(hwdatasactual[0].id01.freq <= element.create.if.right.freq){
          cumpleCond = true
        }

      }

    } else if( element.create.if.right.sensor !== undefined){

      if( element.create.if.condition === '='){

        if(hwdatasactual[0].id01.sensor === element.create.if.right.sensor){
          cumpleCond = true
        }

      } else if( element.create.if.condition === '!='){

        if(hwdatasactual[0].id01.sensor !== element.create.if.right.sensor){
          cumpleCond = true
        }

      } else if( element.create.if.condition === '>'){

        if(hwdatasactual[0].id01.sensor > element.create.if.right.sensor){
          cumpleCond = true
        }

      } else if( element.create.if.condition === '>='){

        if(hwdatasactual[0].id01.sensor >= element.create.if.right.sensor){
          cumpleCond = true
        }

      } else if( element.create.if.condition === '<'){

        if(hwdatasactual[0].id01.sensor < element.create.if.right.sensor){
          cumpleCond = true
        }

      } else if( element.create.if.condition === '<='){

        if(hwdatasactual[0].id01.sensor <= element.create.if.right.sensor){
          cumpleCond = true
        }

      }

    } else if( element.create.if.right.status !== undefined){

      if( element.create.if.condition === '='){

        if( element.create.if.left.id === "id02" ){

          if(hwdatasactual[0].id02.status === element.create.if.right.status){
            cumpleCond = true
          }

        } else if( element.create.if.left.id === "id03" ){

          if(hwdatasactual[0].id03.status === element.create.if.right.status){
            cumpleCond = true
          }

        }

      } else if( element.create.if.condition === '!='){

        if( element.create.if.left.id === "id02" ){

          if(hwdatasactual[0].id02.status !== element.create.if.right.status){
            cumpleCond = true
          }

        } else if( element.create.if.left.id === "id03" ){

          if(hwdatasactual[0].id03.status !== element.create.if.right.status){
            cumpleCond = true
          }

        }

      }

    } else if( element.create.if.right.text !== undefined){

      if( element.create.if.condition === '='){

        if( element.create.if.left.id === "id02" ){

          if(hwdatasactual[0].id02.text === element.create.if.right.text){
            cumpleCond = true
          }

        } else if( element.create.if.left.id === "id03" ){

          if(hwdatasactual[0].id03.text === element.create.if.right.text){
            cumpleCond = true
          }

        }

      } else if( element.create.if.condition === '!='){

        if( element.create.if.left.id === "id02" ){

          if(hwdatasactual[0].id02.text !== element.create.if.right.text){
            cumpleCond = true
          }

        } else if( element.create.if.left.id === "id03" ){

          if(hwdatasactual[0].id03.text !== element.create.if.right.text){
            cumpleCond = true
          }

        }

      }

    }

    if(cumpleCond){

      if(element.create.then !== undefined){

        structureid = `{"${element.create.then.id}": `

        if(element.create.then.freq != undefined ){
          structureid += `{ "freq": ${element.create.then.freq} }}`
        }

        if(element.create.then.status !== undefined && element.create.then.text != undefined){
          structureid += `{ "status": ${element.create.then.status}, `
          structureid += `"text": "${element.create.then.text}" }}`

        } else if(element.create.then.status != undefined){
          structureid += `{ "status": ${element.create.then.status} }}`

        } else if(element.create.then.text != undefined){
          structureid += `{ "text": "${element.create.then.text}" }}`
        }

        structureChange.change = JSON.parse(structureid)

        //Realizando cambios en Hardware
        await fetch(
          `http://${element.create.then.url}/change`,
          {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(structureChange)
        })
        .then( resFetch => resFetch.json())
        .then( datos => { console.log("Change Status: " + datos.status) } )
        .catch( err => console.log("[EVENT HANDLER][ERROR]: Fetch Change Then"))

      }

    } else {

      if(element.create.else !== undefined){

        structureid = `{"${element.create.else.id}": `

        if(element.create.else.freq != undefined ){
          structureid += `{ "freq": ${element.create.else.freq} }}`
        }

        if(element.create.else.status !== undefined && element.create.else.text != undefined){
          structureid += `{ "status": ${element.create.else.status}, `
          structureid += `"text": "${element.create.else.text}" }}`

        } else if(element.create.else.status != undefined){
          structureid += `{ "status": ${element.create.else.status} }} `

        } else if(element.create.else.text != undefined){
          structureid += `{ "text": "${element.create.else.text}" }}`
        }

        structureChange.change = JSON.parse(structureid)

        //Realizando cambios en Hardware
        await fetch(
          `http://${element.create.else.url}/change`,
          {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(structureChange)
        })
        .then( resFetch => resFetch.json())
        .then( datos => { console.log("Change Status: " + datos.status) } )
        .catch( err => console.log("[EVENT HANDLER][ERROR]: Fetch Change Else"))
      }
    }

  });
}
