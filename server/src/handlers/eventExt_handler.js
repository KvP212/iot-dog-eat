import Createdata from '../models/create_event_data'
import {URLLOCAL} from '../globalV'
import {setEvents} from './listEventsExt'

export default async () => {

  console.log("---EXEC EVENTS EXTERNOS---")
  var allEvents = await Createdata.find({"create.if.left.url":{$ne: URLLOCAL}})

  allEvents.forEach( element => {
    setEvents( element )
  })
}
