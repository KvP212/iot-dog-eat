import fetch from 'node-fetch'
import {URLLOCAL} from '../globalV'
import moment from 'moment'

var arrayEvents = []

export function getEvents() {
  return arrayEvents
}

export function setEvents( element ) {

  var Evento = {
    idEvent: element.idEvent,

    initInterval:
      setInterval( async () => {

        var structureChange = {
          "id": "PT_CC8Project212",
          "url": URLLOCAL,
          "date": moment().utc(true).toISOString(),
          "change": {}
        }

        const reqSearch = {
          "id": "PT_CC8Project212",
          "url": URLLOCAL,
          "date": moment().utc(true).toISOString(),
          "search": {
            "id_hardware": element.create.if.left.id,
            "start_date":  moment().milliseconds( -element.create.if.left.freq).utc(true).toISOString(),
            "finish_date": moment().utc(true).toISOString()
          }
        }

        var structureid = ""

        var resSearch = {}

        await fetch('http://' + element.create.if.left.url + '/search',{
          method: "POST",
          body: JSON.stringify(reqSearch),
          headers: {
            "Content-Type": "application/json; charset=utf-8"
          }
        })
        .then( resFetch => resFetch.json())
        .then( datos => {
          resSearch = datos
        })
        .catch( err => console.log("[ERROR][POST] fetch Search to anywhere: " + err))

        var cumpleCond = false

        if(resSearch.length !== 0){
          for(var instante in resSearch.data){

            if( element.create.if.right.freq !== undefined){

              if( element.create.if.condition === '='){

                if(resSearch.data[instante].freq === element.create.if.right.freq){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '!='){

                if(resSearch.data[instante].freq !== element.create.if.right.freq){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '>'){

                if(resSearch.data[instante].freq > element.create.if.right.freq){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '>='){

                if(resSearch.data[instante].freq >= element.create.if.right.freq){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '<'){

                if(resSearch.data[instante].freq < element.create.if.right.freq){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '<='){

                if(resSearch.data[instante].freq <= element.create.if.right.freq){
                  cumpleCond = true
                  break
                }

              }

            } else if( element.create.if.right.sensor !== undefined){

              if( element.create.if.condition === '='){

                if(resSearch.data[instante].sensor === element.create.if.right.sensor){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '!='){

                if(resSearch.data[instante].sensor !== element.create.if.right.sensor){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '>'){

                if(resSearch.data[instante].sensor > element.create.if.right.sensor){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '>='){

                if(resSearch.data[instante].sensor >= element.create.if.right.sensor){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '<'){

                if(resSearch.data[instante].sensor < element.create.if.right.sensor){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '<='){

                if(resSearch.data[instante].sensor <= element.create.if.right.sensor){
                  cumpleCond = true
                  break
                }

              }

            } else if( element.create.if.right.status !== undefined){

              if( element.create.if.condition === '='){

                if(resSearch.data[instante].status === element.create.if.right.status){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '!='){

                if(resSearch.data[instante].status !== element.create.if.right.status){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '>'){

                if(resSearch.data[instante].status > element.create.if.right.status){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '>='){

                if(resSearch.data[instante].status >= element.create.if.right.status){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '<'){

                if(resSearch.data[instante].status < element.create.if.right.status){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '<='){

                if(resSearch.data[instante].status <= element.create.if.right.status){
                  cumpleCond = true
                  break
                }

              }

            } else if( element.create.if.right.text !== undefined){

              if( element.create.if.condition === '='){

                if(resSearch.data[instante].text === element.create.if.right.text){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '!='){

                if(resSearch.data[instante].text !== element.create.if.right.text){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '>'){

                if(resSearch.data[instante].text > element.create.if.right.text){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '>='){

                if(resSearch.data[instante].text >=
                  element.create.if.right.text){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '<'){

                if(resSearch.data[instante].text < element.create.if.right.text){
                  cumpleCond = true
                  break
                }

              } else if( element.create.if.condition === '<='){

                if(resSearch.data[instante].text <= element.create.if.right.text){
                  cumpleCond = true
                  break
                }

              }

            }

          }

          if(cumpleCond){

            if(element.create.then !== undefined){

              structureid = `{"${element.create.then.id}": `

              if(element.create.then.freq != undefined ){
                structureid += `{ "freq": ${element.create.then.freq} }}`
              }

              if(element.create.then.status !== undefined && element.create.then.text != undefined){
                structureid += `{ "status": ${element.create.then.status}, `
                structureid += `"text": "${element.create.then.text}" }}`

              } else if(element.create.then.status != undefined){
                structureid += `{ "status": ${element.create.then.status} }}`

              } else if(element.create.then.text != undefined){
                structureid += `{ "text": "${element.create.then.text}" }}`
              }

              structureChange.change = JSON.parse(structureid)

              //Realizando cambios en Hardware
              await fetch(
                `http://${element.create.then.url}/change`,
                {
                  method: 'POST',
                  headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                  },
                  body: JSON.stringify(structureChange)
              })
              .then( resFetch => resFetch.json())
              .then( datos => { console.log("Change Status: " + datos.status) } )
              .catch( err => console.log("[EVENT EXT][ERROR]: Fetch Change Then"))

            }

          } else {

            if(element.create.else !== undefined){

              structureid = `{"${element.create.else.id}": `

              if(element.create.else.freq != undefined ){
                structureid += `{ "freq": ${element.create.else.freq} }}`
              }

              if(element.create.else.status !== undefined && element.create.else.text != undefined){
                structureid += `{ "status": ${element.create.else.status}, `
                structureid += `"text": "${element.create.else.text}" }}`

              } else if(element.create.else.status != undefined){
                structureid += `{ "status": ${element.create.else.status} }} `

              } else if(element.create.else.text != undefined){
                structureid += `{ "text": "${element.create.else.text}" }}`
              }

              structureChange.change = JSON.parse(structureid)

              //Realizando cambios en Hardware
              await fetch(
                `http://${element.create.else.url}/change`,
                {
                  method: 'POST',
                  headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                  },
                  body: JSON.stringify(structureChange)
              })
              .then( resFetch => resFetch.json())
              .then( datos => { console.log("Change Status: " + datos.status) } )
              .catch( err => console.log("[EVENT EXT][ERROR]: Fetch Change Else"))
            }
          }
        }


      },
      element.create.if.left.freq),

    endInterval: function() { clearInterval(this.initInterval)}
  }

  arrayEvents.push(Evento)
}
