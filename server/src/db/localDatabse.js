import mongoose from 'mongoose';

const URI = 'mongodb://localhost/iotEatDog';
mongoose.connect(URI, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify:false })
    .then(db => console.log('DB is connected'))
    .catch(err => console.log(err));

export default mongoose;
