import { Router } from 'express'
import Createdata from '../models/create_event_data'
import {URLLOCAL} from '../globalV'
import mongoose from '../db/localDatabse'
import {getEvents} from '../handlers/listEventsExt'
import moment from 'moment'
import {token, chat_id} from '../handlers/botIoTNet'
import fetch from 'node-fetch'
const router = Router()

router.post('/', async (req, res) =>{

  var structureRes = {

    "id": "PT_CC8Project212",
    "url": URLLOCAL,
    "date": moment().utc(true).toISOString(),
    "status": "OK"
  }

  const {id, url, date} = req.body
  const del = req.body.delete.id

  console.log("---DELETE---")
  console.log("id: " + id)
  console.log("url: " + url)
  console.log("date: " + date)
  console.log("delete.id: " + del)

  //Eliminando evento
  await Createdata.findByIdAndRemove({_id: mongoose.Types.ObjectId(del)}, (err, resFind) => {
    if(err) {
      console.log(err)
      structureRes.status = "ERROR"
    }

    if(!resFind) {
      console.log("ERROR doc not found for update")
      structureRes.status = "ERROR"
    }
  })

  const messageTelegram = `
      *Evento*+*Eliminado*
      *Url:*+${url}
      *iDEvent*+${del}`

    fetch(`https://api.telegram.org/bot${token}/sendMessage?chat_id=${chat_id}&text=${messageTelegram}&parse_mode=Markdown`)
    .then( resFetch => resFetch.json())
    .then( datos => { console.log("Send Notification Telegram: " + datos.ok) } )
    .catch( err => console.log("[INFO][ERROR]: Fetch SendMessage Telegram"))

  getEvents().forEach( eventExt => {
    if(eventExt.idEvent === del){
      eventExt.endInterval()
    }
  })

  res.status(200).json(structureRes)

})

export default router
