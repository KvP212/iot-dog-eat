import { Router } from 'express'
import Infodata from '../models/info_data'
import ReqInfodata from '../models/req_info_data'
import {token, chat_id} from '../handlers/botIoTNet'
import moment from 'moment'
import fetch from 'node-fetch'
const router = Router()

//hardwares
router.get('/', async (req, res) =>{

  const infodatas = await Infodata.find()
  res.status(200).json(infodatas[0])

})

//POST Info
router.post('/', async (req, res) =>{

  const {id, url, date} = req.body
  console.log("---INFO---")
  console.log("id: " + id)
  console.log("url: " + url)
  console.log("date: " + date)

  const messageTelegram = `
    *Info Request*
    *Url:*+${url}
    *Date:*+${moment(date).utc(false).format('LLL')}`

  fetch(`https://api.telegram.org/bot${token}/sendMessage?chat_id=${chat_id}&text=${messageTelegram}&parse_mode=Markdown`)
  .then( resFetch => resFetch.json())
  .then( datos => { console.log("Send Notification Telegram: " + datos.ok) } )
  .catch( err => console.log("[INFO][ERROR]: Fetch SendMessage Telegram"))

  ReqInfodata.findOneAndUpdate(
    {idProyect: id},
    {url, date},
    {upsert: true},
    function(err, doc){
      if(err) console.log("Error Req Info: " + err)
      console.log("Save Req Info OK")
    }
  )

  const infodatas = await Infodata.find({idPT: "PT_CC8Project212"})
  res.status(200).json(
    {
      "id" : infodatas[0].idPT,
      "url" : infodatas[0].url,
      "date" : infodatas[0].date,
      "hardware" : infodatas[0].hardware
    }
  )

})

//Registrar nuevo hardware
router.post('/newHardware', async (req, res) =>{

  const {idPT, url, date, hardware} = req.body

  await Infodata.findOneAndUpdate(
    {idPT},
    {url, date, hardware},
    {upsert: true},
    (err, doc) => {
      if(err) console.log("Error: " + err)
      console.log("OK")
    }
  )
  res.status(200).json({status: 'Hardware Saved'})

})

export default router
