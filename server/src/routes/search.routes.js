import { Router } from 'express'
import Hwpesodata from '../models/hw_peso_data'
import Infodata from '../models/info_data'
import ReqSearchdata from '../models/req_search_data'
import moment from 'moment'
import {token, chat_id} from '../handlers/botIoTNet'
import fetch from 'node-fetch'
const router = Router()

//POST Search
router.post('/', async (req, res) =>{

  const {id, url, date, search} = req.body
  console.log("---SEARCH---")
  console.log("id: " + id)
  console.log("url: " + url)
  console.log("date: " + date)

  const messageTelegram = `
    *Search*
    *Url:*+${url}
    *Start_date:*+${moment(search.start_date).utc(false).format('LLL')}
    *Finish_date:*+${moment(search.finish_date).utc(false).format('LLL')}`

  fetch(`https://api.telegram.org/bot${token}/sendMessage?chat_id=${chat_id}&text=${messageTelegram}&parse_mode=Markdown`)
  .then( resFetch => resFetch.json())
  .then( datos => { console.log("Send Notification Telegram: " + datos.ok) } )
  .catch( err => console.log("[SEARCH][ERROR]: Fetch SendMessage Telegram"))

  ReqSearchdata.findOneAndUpdate(
    {idPT: id},
    {url, date, search},
    {upsert: true},
    function(err, doc){
      if(err) console.log("Error Req Search: " + err)
      console.log('Save Req Search OK')
    }
  )

  //Res structure
  const structureRes = {
    "id": "",
    "url": "",
    "date": "",
    "search": {
      "id_hardware": search.id_hardware,
      "type": ""
    },
    "data": {}
  }

  const infodatas = await Infodata.find({idPT: "PT_CC8Project212"})

  structureRes.id = infodatas[0].idPT
  structureRes.url = infodatas[0].url
  structureRes.date = moment().utcOffset(-360)._d

  console.log(`SS: ${search.start_date}`)
  console.log(`SF: ${search.finish_date}`)
  const hwdatas = await Hwpesodata.find(
    {
      date: {
        $gt: moment(search.start_date).utcOffset(360)._d,
        $lt: moment(search.finish_date).utcOffset(360)._d
      }
    }
  )

  var data = "{"
  if(search.id_hardware === "id01"){
    structureRes.search.type = infodatas[0].hardware.id01.type
    for(var i = 0; i < hwdatas.length ; i++){
      data +=
      `"${hwdatas[i].date.toISOString()}":
        {
          "sensor": ${hwdatas[i].id01.sensor},
          "freq": ${hwdatas[i].id01.freq}
        }`

      if(i !== hwdatas.length - 1){
        data += ","
      }
    }

  } else if (search.id_hardware === "id02"){
    structureRes.search.type = infodatas[0].hardware.id02.type
    for(var i = 0; i < hwdatas.length ; i++){
      data +=
      `"${hwdatas[i].date.toISOString()}":
        {
          "status": ${hwdatas[i].id02.status},
          "text": "${hwdatas[i].id02.text}"
        }`

      if(i !== hwdatas.length - 1){
        data += ","
      }
    }

  } else if (search.id_hardware === "id03"){
    structureRes.search.type = infodatas[0].hardware.id03.type
    for(var i = 0; i < hwdatas.length ; i++){
      data +=
      `"${hwdatas[i].date.toISOString()}":
        {
          "status": ${hwdatas[i].id03.status},
          "text": "${hwdatas[i].id03.text}"
        }`

      if(i !== hwdatas.length - 1){
        data += ","
      }
    }

  }

  data += "}"
  structureRes.data = JSON.parse(data)
  res.status(200).json(structureRes)
})

//Save Search
router.post('/saveSearch', async (req, res) =>{

  const {id, url, date, search} = req.body
  console.log("---SEARCH SAVE---")
  console.log("id: " + id)
  console.log("url: " + url)
  console.log("date: " + date)

  console.log(`SSC:`+ new Date(search.start_date).toISOString())
  console.log(`SFC:` + new Date(search.finish_date).toISOString())
  const searchdatas = await Hwpesodata.find(
    {
      "date": {
        $gt: new Date(search.start_date),
        $lt: new Date(search.finish_date)}
    }
  )

  res.status(200).json(searchdatas)
})

export default router
