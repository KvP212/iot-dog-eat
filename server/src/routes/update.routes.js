import { Router } from 'express'
import Createdata from '../models/create_event_data'
import {URLLOCAL} from '../globalV'
import mongoose from '../db/localDatabse'
import {getEvents, setEvents} from '../handlers/listEventsExt'
import moment from 'moment'
import {token, chat_id} from '../handlers/botIoTNet'
import fetch from 'node-fetch'
const router = Router()

router.post('/', async (req, res) =>{

  var structureRes = {

    "id": "PT_CC8Project212",
    "url": URLLOCAL,
    "date": moment().utc(true).toISOString(),
    "status": "OK"
  }

  const {id, url, date, update} = req.body
  console.log("---UPDATE---")
  console.log("id: " + id)
  console.log("url: " + url)
  console.log("date: " + date)
  console.log("update.id: " + update.id)

  const docFound = await Createdata.findById({_id: mongoose.Types.ObjectId(update.id)}, (err, resFind) => {
    if(err) {
      console.log(err)
      structureRes.status = "ERROR"
    }

    if(!resFind) {
      console.log("ERROR doc not found for update")
      structureRes.status = "ERROR"
    }
  })

  var createToUpdate = {
    if: {},
    then: {},
    else: {}
  }

  if(update.if) { createToUpdate.if = update.if }
  else { createToUpdate.if = docFound.create.if }

  if(update.then) {createToUpdate.then =  update.then }
  else { createToUpdate.then = docFound.create.then }

  if(update.else) {createToUpdate.else = update.else}
  else { createToUpdate.else = docFound.create.else }

  //Actualizando evento
  await docFound.updateOne({create: createToUpdate})

  const messageTelegram = `
      *Evento*+*Actualizado*
      *Url:*+${url}
      *iDEvent*+${update.id}`

    fetch(`https://api.telegram.org/bot${token}/sendMessage?chat_id=${chat_id}&text=${messageTelegram}&parse_mode=Markdown`)
    .then( resFetch => resFetch.json())
    .then( datos => { console.log("Send Notification Telegram: " + datos.ok) } )
    .catch( err => console.log("[INFO][ERROR]: Fetch SendMessage Telegram"))

  getEvents().forEach( eventExt => {
    if(eventExt.idEvent === update.id){
      eventExt.endInterval()
    }
  })

  const docFoundUpdate = await Createdata.findById({_id: mongoose.Types.ObjectId(update.id)})
  if(docFoundUpdate.create.if.left.url !== URLLOCAL){
    setEvents(docFoundUpdate)
  }

  res.status(200).json(structureRes)

})

export default router
