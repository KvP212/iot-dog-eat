import { Router } from 'express';
import Hwpesodata from '../models/hw_peso_data'
import Reshwdata from '../models/res_hw_data'
import * as eventHandlers from '../handlers/event_handlers'
const router = Router();


//Todos los pesos registrados
router.get('/', async (req, res) =>{
  const getHw = await Reshwdata.findOne()

  res.status(200).json(
    {
      freq: getHw.freq,
      statusMotor: getHw.statusMotor,
      statusLED: getHw.statusLED,
      textMotor: getHw.textMotor,
      textLED: getHw.textLED
    }
  )

});

//Registrar nuevo peso
router.post('/', async (req, res) =>{

  const {sensor, freq, statusMotor, textMotor, statusLED, textLED} = req.body

  const hw01 = { sensor, freq }
  const hw02 = { status: statusMotor, text: textMotor }
  const hw03 = { status: statusLED, text: textLED }

  const hwdata = new Hwpesodata({
    id01: hw01,
    id02: hw02,
    id03: hw03
  })

  await hwdata.save()

  eventHandlers.default()

  res.status(200).json({})

});

export default router;
