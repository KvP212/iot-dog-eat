import { Router } from 'express'
import Createdata from '../models/create_event_data'
import {URLLOCAL} from '../globalV'
import {setEvents} from '../handlers/listEventsExt'
import moment from 'moment'
import {token, chat_id} from '../handlers/botIoTNet'
import fetch from 'node-fetch'
const router = Router()

router.post('/', async (req, res) =>{

  var structureRes = {

    "id": "PT_CC8Project212",
    "url": URLLOCAL,
    "date": moment().utc(true).toISOString(),
    "status": "OK",
    "idEvent": ""
  }

  const {id, url, date, create} = req.body
  console.log("---CREATE---")
  console.log("id: " + id)
  console.log("url: " + url)
  console.log("date: " + date)
  console.log("create: " + create)

  const createdatas = new Createdata({
    idFE: id,
    url,
    date,
    create
  })

  //Guardando evento creado
  await createdatas.save()

  const filter = { idFE: id, date}

  const docFound = await Createdata.findOne(filter, (err, resFind) => {

    if(err) {
      console.log(err)
      structureRes.status = "ERROR"
    }

    if(!resFind) {
      console.log("ERROR")
      structureRes.status = "ERROR"
    }

    structureRes.idEvent = resFind._id

    const messageTelegram = `
      *Evento*+*Creado*
      *Url:*+${url}
      *iDEvent*+${resFind._id}`

    fetch(`https://api.telegram.org/bot${token}/sendMessage?chat_id=${chat_id}&text=${messageTelegram}&parse_mode=Markdown`)
    .then( resFetch => resFetch.json())
    .then( datos => { console.log("Send Notification Telegram: " + datos.ok) } )
    .catch( err => console.log("[INFO][ERROR]: Fetch SendMessage Telegram"))

  })

  //Actualizando idEvent del event
  await docFound.updateOne({idEvent: docFound._id})

  if(create.if.left.url !== URLLOCAL){
    const docFoundUpdate = await Createdata.findOne(filter)
    setEvents(docFoundUpdate)
  }

  //Enviando estrucura del response con idEvent incluido
  res.status(200).json(structureRes)

})

export default router
