import { Router } from 'express'
import Infodata from '../models/info_data'
import Reshwdata from '../models/res_hw_data'
import moment from 'moment'
import ReqChangedata from '../models/req_change_data'
import {token, chat_id} from '../handlers/botIoTNet'
import fetch from 'node-fetch'
const router = Router()

//POST Search
router.post('/', async (req, res) =>{

  const {id, url, date, change} = req.body
  console.log("---CHANGE---")
  console.log("id: " + id)
  console.log("url: " + url)
  console.log("date: " + date)
  console.log(change.id01)
  console.log(change.id02)
  console.log(change.id03)

  ReqChangedata.findOneAndUpdate(
    {idPT: id},
    {url, date, change},
    {upsert: true},
    function(err, doc){
      if(err) console.log("Error Req Change: " + err)
      console.log('Save Req Change OK')
    }
  )
  var messageTelegram = `
  *Change*+*en*+*Hardware*
  *Url:*+${url}\n`

  const updateHw = await Reshwdata.findOne()

  if(change.id01 !== undefined){
    if(change.id01.freq !== undefined){
      updateHw.freq = change.id01.freq
      messageTelegram += ` Freq+del+Sensor+a+*${change.id01.freq}ms*`
    }
  }

  if(change.id02  !== undefined){
    if(change.id02.status !== undefined){
      updateHw.statusMotor = change.id02.status
      messageTelegram += ` Status+del+Motor+a+*${change.id02.status}*\n`
    }

    if(change.id02.text !== undefined){
      updateHw.textMotor = change.id02.text
      messageTelegram += ` Texto+del+Motor+a+*${change.id02.text}*\n`
    }
  }

  if(change.id03  !== undefined){
    if(change.id03.status !== undefined){
      updateHw.statusLED = change.id03.status
      messageTelegram += ` Status+del+LED+a+*${change.id03.status}*\n`
    }

    if(change.id03.text !== undefined){
      updateHw.textLED = change.id03.text
      messageTelegram += ` Texto+del+LED+a+*${change.id03.text}*\n`
    }
  }

  await updateHw.save()

  fetch(`https://api.telegram.org/bot${token}/sendMessage?chat_id=${chat_id}&text=${messageTelegram}&parse_mode=Markdown`)
  .then( resFetch => resFetch.json())
  .then( datos => { console.log("Send Notification Telegram: " + datos.ok) } )
  .catch( err => console.log("[CHANGE][ERROR]: Fetch SendMessage Telegram"))

  const infodatas = await Infodata.find()

  //Res structure
  const structureRes = {
    "id": "",
    "url": "",
    "date": "",
    "status": ""
  }

  structureRes.id = infodatas[0].idPT
  structureRes.url = infodatas[0].url
  structureRes.date = moment().utcOffset(-360)._d
  structureRes.status = "OK"

  res.status(200).json(structureRes)

})

export default router
