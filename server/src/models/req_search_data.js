import mongoose, { Schema } from "mongoose"

const reqsearchdataSchema = new Schema({
  idPT:
    {
      type: String
    },

  url:
    {
      type: String
    },

  date:
    {
      type: Date,
      default: Date.now
    },

  search:
    {
      type: Object
    }
})

export default mongoose.model('ReqSearchdata', reqsearchdataSchema)
