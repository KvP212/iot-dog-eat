import mongoose, { Schema } from "mongoose"

const reqinfodataSchema = new Schema({
  idProyect:
    {
      type: String
    },

  url:
    {
      type: String
    },

  date:
    {
      type: Date,
      default: Date.now
    }
})

export default mongoose.model('ReqInfodata', reqinfodataSchema)
