import mongoose, { Schema } from "mongoose"

const reqchangedataSchema = new Schema({
  idPT:
    {
      type: String
    },

  url:
    {
      type: String
    },

  date:
    {
      type: Date,
      default: Date.now
    },

  change:
    {
      type: Object
    }
})

export default mongoose.model('ReqChangedata', reqchangedataSchema)
