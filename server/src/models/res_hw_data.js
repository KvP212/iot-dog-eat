import mongoose, { Schema } from "mongoose"

const reshwdataSchema = new Schema({
  freq:
    {
      type: Number
    },

  statusMotor:
    {
      type: Boolean
    },

  textMotor:
    {
      type: String
    },

  statusLED:
    {
      type: Boolean
    },

  textLED:
    {
      type: String
    }
})

export default mongoose.model('Reshwdata', reshwdataSchema)
